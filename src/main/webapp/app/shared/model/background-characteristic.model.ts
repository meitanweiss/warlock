import { IBackground } from 'app/shared/model/background.model';

export interface IBackgroundCharacteristic {
    id?: number;
    description?: string;
    background?: IBackground;
}

export class BackgroundCharacteristic implements IBackgroundCharacteristic {
    constructor(public id?: number, public description?: string, public background?: IBackground) {}
}
