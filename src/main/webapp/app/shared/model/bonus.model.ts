import { ICharacterClass } from 'app/shared/model/character-class.model';
import { IRacialTraits } from 'app/shared/model/racial-traits.model';

export interface IBonus {
    id?: number;
    name?: string;
    change?: number;
    stat?: string;
    characterClass?: ICharacterClass;
    racialTraits?: IRacialTraits;
}

export class Bonus implements IBonus {
    constructor(
        public id?: number,
        public name?: string,
        public change?: number,
        public stat?: string,
        public characterClass?: ICharacterClass,
        public racialTraits?: IRacialTraits
    ) {}
}
