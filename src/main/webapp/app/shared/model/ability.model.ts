import { ICharacterClass } from 'app/shared/model/character-class.model';

export interface IAbility {
    id?: number;
    name?: string;
    description?: string;
    characterClass?: ICharacterClass;
}

export class Ability implements IAbility {
    constructor(public id?: number, public name?: string, public description?: string, public characterClass?: ICharacterClass) {}
}
