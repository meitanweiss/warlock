import { IBackground } from 'app/shared/model/background.model';
import { ICharacterClass } from 'app/shared/model/character-class.model';

export interface IProficiency {
    id?: number;
    name?: string;
    description?: string;
    background?: IBackground;
    characterClass?: ICharacterClass;
}

export class Proficiency implements IProficiency {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public background?: IBackground,
        public characterClass?: ICharacterClass
    ) {}
}
