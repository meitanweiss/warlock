import { IBackground } from 'app/shared/model/background.model';

export interface ITrait {
    id?: number;
    name?: string;
    description?: string;
    background?: IBackground;
}

export class Trait implements ITrait {
    constructor(public id?: number, public name?: string, public description?: string, public background?: IBackground) {}
}
