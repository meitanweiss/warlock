import { IProficiency } from 'app/shared/model/proficiency.model';
import { IAbility } from 'app/shared/model/ability.model';
import { IBonus } from 'app/shared/model/bonus.model';
import { IChara } from 'app/shared/model/chara.model';

export interface ICharacterClass {
    id?: number;
    name?: string;
    description?: string;
    hp?: string;
    proficiencies?: IProficiency[];
    abilities?: IAbility[];
    bonuses?: IBonus[];
    charas?: IChara[];
}

export class CharacterClass implements ICharacterClass {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public hp?: string,
        public proficiencies?: IProficiency[],
        public abilities?: IAbility[],
        public bonuses?: IBonus[],
        public charas?: IChara[]
    ) {}
}
