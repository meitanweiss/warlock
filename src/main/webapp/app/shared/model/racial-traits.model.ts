import { IRace } from 'app/shared/model/race.model';
import { IBonus } from 'app/shared/model/bonus.model';

export interface IRacialTraits {
    id?: number;
    name?: string;
    description?: string;
    race?: IRace;
    bonuses?: IBonus[];
}

export class RacialTraits implements IRacialTraits {
    constructor(public id?: number, public name?: string, public description?: string, public race?: IRace, public bonuses?: IBonus[]) {}
}
