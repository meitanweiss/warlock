import { IRacialTraits } from 'app/shared/model/racial-traits.model';

export interface IRace {
    id?: number;
    name?: string;
    summary?: string;
    description?: string;
    age?: string;
    alignment?: string;
    size?: string;
    speed?: string;
    languages?: string;
    racialTraits?: IRacialTraits[];
}

export class Race implements IRace {
    constructor(
        public id?: number,
        public name?: string,
        public summary?: string,
        public description?: string,
        public age?: string,
        public alignment?: string,
        public size?: string,
        public speed?: string,
        public languages?: string,
        public racialTraits?: IRacialTraits[]
    ) {}
}
