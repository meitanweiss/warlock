import { IChara } from 'app/shared/model/chara.model';

export interface IStats {
    id?: number;
    name?: string;
    num?: number;
    abbr?: string;
    chara?: IChara;
}

export class Stats implements IStats {
    constructor(public id?: number, public name?: string, public num?: number, public abbr?: string, public chara?: IChara) {}
}
