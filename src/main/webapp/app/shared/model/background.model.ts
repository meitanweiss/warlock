import { IProficiency } from 'app/shared/model/proficiency.model';
import { IChara } from 'app/shared/model/chara.model';
import { ITrait } from 'app/shared/model/trait.model';
import { IBackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';

export interface IBackground {
    id?: number;
    name?: string;
    description?: string;
    proficiencies?: IProficiency[];
    charas?: IChara[];
    traits?: ITrait[];
    backgroundCharacteristics?: IBackgroundCharacteristic[];
}

export class Background implements IBackground {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public proficiencies?: IProficiency[],
        public charas?: IChara[],
        public traits?: ITrait[],
        public backgroundCharacteristics?: IBackgroundCharacteristic[]
    ) {}
}
