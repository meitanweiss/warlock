import { IChara } from 'app/shared/model/chara.model';

export interface IUserProfile {
    id?: number;
    userId?: number;
    charas?: IChara[];
}

export class UserProfile implements IUserProfile {
    constructor(public id?: number, public userId?: number, public charas?: IChara[]) {}
}
