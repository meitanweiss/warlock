import { IStats } from 'app/shared/model/stats.model';
import { IUserProfile } from 'app/shared/model/user-profile.model';
import { IBackground } from 'app/shared/model/background.model';
import { ICharacterClass } from 'app/shared/model/character-class.model';
import { IRace } from 'app/shared/model/race.model';

export interface IChara {
    id?: number;
    name?: string;
    alingment?: string;
    faith?: string;
    lifestyle?: string;
    hair?: string;
    skin?: string;
    eyes?: string;
    height?: number;
    weight?: number;
    age?: number;
    flaws?: string;
    special?: string;
    gender?: string;
    peronality?: string;
    ideals?: string;
    bonds?: string;
    stats?: IStats[];
    userProfile?: IUserProfile;
    background?: IBackground;
    characterClass?: ICharacterClass;
    race?: IRace;
}

export class Chara implements IChara {
    constructor(
        public id?: number,
        public name?: string,
        public alingment?: string,
        public faith?: string,
        public lifestyle?: string,
        public hair?: string,
        public skin?: string,
        public eyes?: string,
        public height?: number,
        public weight?: number,
        public age?: number,
        public flaws?: string,
        public special?: string,
        public gender?: string,
        public peronality?: string,
        public ideals?: string,
        public bonds?: string,
        public stats?: IStats[],
        public userProfile?: IUserProfile,
        public background?: IBackground,
        public characterClass?: ICharacterClass,
        public race?: IRace
    ) {}
}
