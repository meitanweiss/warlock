import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IStats } from 'app/shared/model/stats.model';
import { StatsService } from './stats.service';
import { IChara } from 'app/shared/model/chara.model';
import { CharaService } from 'app/entities/chara';

@Component({
    selector: 'my-stats-update',
    templateUrl: './stats-update.component.html'
})
export class StatsUpdateComponent implements OnInit {
    stats: IStats;
    isSaving: boolean;

    charas: IChara[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected statsService: StatsService,
        protected charaService: CharaService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ stats }) => {
            this.stats = stats;
        });
        this.charaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IChara[]>) => mayBeOk.ok),
                map((response: HttpResponse<IChara[]>) => response.body)
            )
            .subscribe((res: IChara[]) => (this.charas = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.stats.id !== undefined) {
            this.subscribeToSaveResponse(this.statsService.update(this.stats));
        } else {
            this.subscribeToSaveResponse(this.statsService.create(this.stats));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IStats>>) {
        result.subscribe((res: HttpResponse<IStats>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCharaById(index: number, item: IChara) {
        return item.id;
    }
}
