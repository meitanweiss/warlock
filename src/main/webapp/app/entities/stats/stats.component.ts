import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IStats } from 'app/shared/model/stats.model';
import { AccountService } from 'app/core';
import { StatsService } from './stats.service';

@Component({
    selector: 'my-stats',
    templateUrl: './stats.component.html'
})
export class StatsComponent implements OnInit, OnDestroy {
    stats: IStats[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected statsService: StatsService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.statsService
            .query()
            .pipe(
                filter((res: HttpResponse<IStats[]>) => res.ok),
                map((res: HttpResponse<IStats[]>) => res.body)
            )
            .subscribe(
                (res: IStats[]) => {
                    this.stats = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInStats();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IStats) {
        return item.id;
    }

    registerChangeInStats() {
        this.eventSubscriber = this.eventManager.subscribe('statsListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
