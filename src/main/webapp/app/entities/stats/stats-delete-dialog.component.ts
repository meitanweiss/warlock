import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStats } from 'app/shared/model/stats.model';
import { StatsService } from './stats.service';

@Component({
    selector: 'my-stats-delete-dialog',
    templateUrl: './stats-delete-dialog.component.html'
})
export class StatsDeleteDialogComponent {
    stats: IStats;

    constructor(protected statsService: StatsService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.statsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'statsListModification',
                content: 'Deleted an stats'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'my-stats-delete-popup',
    template: ''
})
export class StatsDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ stats }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(StatsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.stats = stats;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/stats', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/stats', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
