import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    StatsComponent,
    StatsDetailComponent,
    StatsUpdateComponent,
    StatsDeletePopupComponent,
    StatsDeleteDialogComponent,
    statsRoute,
    statsPopupRoute
} from './';

const ENTITY_STATES = [...statsRoute, ...statsPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [StatsComponent, StatsDetailComponent, StatsUpdateComponent, StatsDeleteDialogComponent, StatsDeletePopupComponent],
    entryComponents: [StatsComponent, StatsUpdateComponent, StatsDeleteDialogComponent, StatsDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockStatsModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
