import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Stats } from 'app/shared/model/stats.model';
import { StatsService } from './stats.service';
import { StatsComponent } from './stats.component';
import { StatsDetailComponent } from './stats-detail.component';
import { StatsUpdateComponent } from './stats-update.component';
import { StatsDeletePopupComponent } from './stats-delete-dialog.component';
import { IStats } from 'app/shared/model/stats.model';

@Injectable({ providedIn: 'root' })
export class StatsResolve implements Resolve<IStats> {
    constructor(private service: StatsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IStats> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Stats>) => response.ok),
                map((stats: HttpResponse<Stats>) => stats.body)
            );
        }
        return of(new Stats());
    }
}

export const statsRoute: Routes = [
    {
        path: '',
        component: StatsComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.stats.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: StatsDetailComponent,
        resolve: {
            stats: StatsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.stats.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: StatsUpdateComponent,
        resolve: {
            stats: StatsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.stats.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: StatsUpdateComponent,
        resolve: {
            stats: StatsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.stats.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const statsPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: StatsDeletePopupComponent,
        resolve: {
            stats: StatsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.stats.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
