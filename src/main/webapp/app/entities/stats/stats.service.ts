import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IStats } from 'app/shared/model/stats.model';

type EntityResponseType = HttpResponse<IStats>;
type EntityArrayResponseType = HttpResponse<IStats[]>;

@Injectable({ providedIn: 'root' })
export class StatsService {
    public resourceUrl = SERVER_API_URL + 'api/stats';

    constructor(protected http: HttpClient) {}

    create(stats: IStats): Observable<EntityResponseType> {
        return this.http.post<IStats>(this.resourceUrl, stats, { observe: 'response' });
    }

    update(stats: IStats): Observable<EntityResponseType> {
        return this.http.put<IStats>(this.resourceUrl, stats, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IStats>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IStats[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
