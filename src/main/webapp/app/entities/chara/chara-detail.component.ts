import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChara } from 'app/shared/model/chara.model';

@Component({
    selector: 'jhi-chara-detail',
    templateUrl: './chara-detail.component.html'
})
export class CharaDetailComponent implements OnInit {
    chara: IChara;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ chara }) => {
            this.chara = chara;
        });
    }

    previousState() {
        window.history.back();
    }
}
