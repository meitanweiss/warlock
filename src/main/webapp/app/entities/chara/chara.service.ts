import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IChara } from 'app/shared/model/chara.model';

type EntityResponseType = HttpResponse<IChara>;
type EntityArrayResponseType = HttpResponse<IChara[]>;

@Injectable({ providedIn: 'root' })
export class CharaService {
    public resourceUrl = SERVER_API_URL + 'api/charas';

    constructor(protected http: HttpClient) {}

    create(chara: IChara): Observable<EntityResponseType> {
        return this.http.post<IChara>(this.resourceUrl, chara, { observe: 'response' });
    }

    update(chara: IChara): Observable<EntityResponseType> {
        return this.http.put<IChara>(this.resourceUrl, chara, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IChara>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IChara[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
