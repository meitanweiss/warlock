import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IChara } from 'app/shared/model/chara.model';
import { AccountService } from 'app/core';
import { CharaService } from './chara.service';

@Component({
    selector: 'jhi-chara',
    templateUrl: './chara.component.html'
})
export class CharaComponent implements OnInit, OnDestroy {
    charas: IChara[];
    currentAccount: any;
    eventSubscriber: Subscription;
    userId: any;

    constructor(
        protected charaService: CharaService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.charaService
            .query()
            .pipe(
                filter((res: HttpResponse<IChara[]>) => res.ok),
                map((res: HttpResponse<IChara[]>) => res.body)
            )
            .subscribe(
                (res: IChara[]) => {
                    this.charas = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCharas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IChara) {
        return item.id;
    }

    registerChangeInCharas() {
        this.eventSubscriber = this.eventManager.subscribe('charaListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
