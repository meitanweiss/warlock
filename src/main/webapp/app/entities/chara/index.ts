export * from './chara.service';
export * from './chara-update.component';
export * from './chara-delete-dialog.component';
export * from './chara-detail.component';
export * from './chara.component';
export * from './chara.route';
