import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IChara } from 'app/shared/model/chara.model';
import { CharaService } from './chara.service';

@Component({
    selector: 'jhi-chara-delete-dialog',
    templateUrl: './chara-delete-dialog.component.html'
})
export class CharaDeleteDialogComponent {
    chara: IChara;

    constructor(protected charaService: CharaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.charaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'charaListModification',
                content: 'Deleted an chara'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-chara-delete-popup',
    template: ''
})
export class CharaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ chara }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CharaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.chara = chara;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/chara', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/chara', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
