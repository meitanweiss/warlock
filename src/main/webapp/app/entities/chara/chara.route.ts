import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Chara } from 'app/shared/model/chara.model';
import { CharaService } from './chara.service';
import { CharaComponent } from './chara.component';
import { CharaDetailComponent } from './chara-detail.component';
import { CharaUpdateComponent } from './chara-update.component';
import { CharaDeletePopupComponent } from './chara-delete-dialog.component';
import { IChara } from 'app/shared/model/chara.model';

@Injectable({ providedIn: 'root' })
export class CharaResolve implements Resolve<IChara> {
    constructor(private service: CharaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IChara> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Chara>) => response.ok),
                map((chara: HttpResponse<Chara>) => chara.body)
            );
        }
        return of(new Chara());
    }
}

export const charaRoute: Routes = [
    {
        path: '',
        component: CharaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'warlockApp.chara.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CharaDetailComponent,
        resolve: {
            chara: CharaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'warlockApp.chara.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CharaUpdateComponent,
        resolve: {
            chara: CharaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'warlockApp.chara.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CharaUpdateComponent,
        resolve: {
            chara: CharaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'warlockApp.chara.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const charaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CharaDeletePopupComponent,
        resolve: {
            chara: CharaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'warlockApp.chara.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
