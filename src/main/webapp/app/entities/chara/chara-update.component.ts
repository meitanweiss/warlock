import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IChara } from 'app/shared/model/chara.model';
import { CharaService } from './chara.service';
import { IUserProfile } from 'app/shared/model/user-profile.model';
import { UserProfileService } from 'app/entities/user-profile';
import { IBackground } from 'app/shared/model/background.model';
import { BackgroundService } from 'app/entities/background';
import { ICharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from 'app/entities/character-class';
import { IRace } from 'app/shared/model/race.model';
import { RaceService } from 'app/entities/race';
import { AccountService } from 'app/core';

@Component({
    selector: 'jhi-chara-update',
    templateUrl: './chara-update.component.html'
})
export class CharaUpdateComponent implements OnInit {
    chara: IChara;
    isSaving: boolean;

    userprofiles: IUserProfile[];

    backgrounds: IBackground[];

    characterclasses: ICharacterClass[];

    races: IRace[];

    racePage: boolean;

    classPage: boolean;

    charaPage: boolean;

    race: IRace;

    currentUser: any;

    finalProfile: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected charaService: CharaService,
        protected userProfileService: UserProfileService,
        protected backgroundService: BackgroundService,
        protected characterClassService: CharacterClassService,
        protected raceService: RaceService,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.charaPage = false;
        this.classPage = false;
        this.racePage = true;
        this.activatedRoute.data.subscribe(({ chara }) => {
            this.chara = chara;
        });
        this.userProfileService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUserProfile[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUserProfile[]>) => response.body)
            )
            .subscribe((res: IUserProfile[]) => (this.userprofiles = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.backgroundService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBackground[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBackground[]>) => response.body)
            )
            .subscribe((res: IBackground[]) => (this.backgrounds = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.characterClassService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICharacterClass[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICharacterClass[]>) => response.body)
            )
            .subscribe((res: ICharacterClass[]) => (this.characterclasses = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.raceService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IRace[]>) => mayBeOk.ok),
                map((response: HttpResponse<IRace[]>) => response.body)
            )
            .subscribe((res: IRace[]) => (this.races = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.chara.id !== undefined) {
            this.subscribeToSaveResponse(this.charaService.update(this.chara));
        } else {
            this.subscribeToSaveResponse(this.charaService.create(this.chara));
        }
    }

    chooseRace(race) {
        this.chara.race = race;
    }

    chooseClass(characterClass) {
        this.chara.characterClass = characterClass;
    }

    saveUserProfile() {
        this.accountService.identity().then(account => {
            this.currentUser = account;
            this.userprofiles.forEach(userProfile => {
                if (this.currentUser.id == userProfile.userId) {
                    this.finalProfile = userProfile;
                    this.chara.userProfile = this.finalProfile;
                }
            });
        });
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IChara>>) {
        result.subscribe((res: HttpResponse<IChara>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserProfileById(index: number, item: IUserProfile) {
        return item.id;
    }

    trackBackgroundById(index: number, item: IBackground) {
        return item.id;
    }

    trackCharacterClassById(index: number, item: ICharacterClass) {
        return item.id;
    }

    trackRaceById(index: number, item: IRace) {
        return item.id;
    }
}
