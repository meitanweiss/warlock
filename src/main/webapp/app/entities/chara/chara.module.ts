import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    CharaComponent,
    CharaDetailComponent,
    CharaUpdateComponent,
    CharaDeletePopupComponent,
    CharaDeleteDialogComponent,
    charaRoute,
    charaPopupRoute
} from './';

const ENTITY_STATES = [...charaRoute, ...charaPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [CharaComponent, CharaDetailComponent, CharaUpdateComponent, CharaDeleteDialogComponent, CharaDeletePopupComponent],
    entryComponents: [CharaComponent, CharaUpdateComponent, CharaDeleteDialogComponent, CharaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockCharaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
