import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITrait } from 'app/shared/model/trait.model';

type EntityResponseType = HttpResponse<ITrait>;
type EntityArrayResponseType = HttpResponse<ITrait[]>;

@Injectable({ providedIn: 'root' })
export class TraitService {
    public resourceUrl = SERVER_API_URL + 'api/traits';

    constructor(protected http: HttpClient) {}

    create(trait: ITrait): Observable<EntityResponseType> {
        return this.http.post<ITrait>(this.resourceUrl, trait, { observe: 'response' });
    }

    update(trait: ITrait): Observable<EntityResponseType> {
        return this.http.put<ITrait>(this.resourceUrl, trait, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ITrait>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITrait[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
