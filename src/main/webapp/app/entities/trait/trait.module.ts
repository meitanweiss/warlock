import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    TraitComponent,
    TraitDetailComponent,
    TraitUpdateComponent,
    TraitDeletePopupComponent,
    TraitDeleteDialogComponent,
    traitRoute,
    traitPopupRoute
} from './';

const ENTITY_STATES = [...traitRoute, ...traitPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [TraitComponent, TraitDetailComponent, TraitUpdateComponent, TraitDeleteDialogComponent, TraitDeletePopupComponent],
    entryComponents: [TraitComponent, TraitUpdateComponent, TraitDeleteDialogComponent, TraitDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockTraitModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
