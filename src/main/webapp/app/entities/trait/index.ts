export * from './trait.service';
export * from './trait-update.component';
export * from './trait-delete-dialog.component';
export * from './trait-detail.component';
export * from './trait.component';
export * from './trait.route';
