import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITrait } from 'app/shared/model/trait.model';
import { AccountService } from 'app/core';
import { TraitService } from './trait.service';

@Component({
    selector: 'jhi-trait',
    templateUrl: './trait.component.html'
})
export class TraitComponent implements OnInit, OnDestroy {
    traits: ITrait[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected traitService: TraitService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.traitService
            .query()
            .pipe(
                filter((res: HttpResponse<ITrait[]>) => res.ok),
                map((res: HttpResponse<ITrait[]>) => res.body)
            )
            .subscribe(
                (res: ITrait[]) => {
                    this.traits = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInTraits();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ITrait) {
        return item.id;
    }

    registerChangeInTraits() {
        this.eventSubscriber = this.eventManager.subscribe('traitListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
