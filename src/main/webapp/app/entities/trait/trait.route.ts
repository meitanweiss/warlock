import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Trait } from 'app/shared/model/trait.model';
import { TraitService } from './trait.service';
import { TraitComponent } from './trait.component';
import { TraitDetailComponent } from './trait-detail.component';
import { TraitUpdateComponent } from './trait-update.component';
import { TraitDeletePopupComponent } from './trait-delete-dialog.component';
import { ITrait } from 'app/shared/model/trait.model';

@Injectable({ providedIn: 'root' })
export class TraitResolve implements Resolve<ITrait> {
    constructor(private service: TraitService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITrait> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Trait>) => response.ok),
                map((trait: HttpResponse<Trait>) => trait.body)
            );
        }
        return of(new Trait());
    }
}

export const traitRoute: Routes = [
    {
        path: '',
        component: TraitComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.trait.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: TraitDetailComponent,
        resolve: {
            trait: TraitResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.trait.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: TraitUpdateComponent,
        resolve: {
            trait: TraitResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.trait.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: TraitUpdateComponent,
        resolve: {
            trait: TraitResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.trait.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const traitPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: TraitDeletePopupComponent,
        resolve: {
            trait: TraitResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.trait.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
