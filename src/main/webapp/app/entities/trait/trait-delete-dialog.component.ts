import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITrait } from 'app/shared/model/trait.model';
import { TraitService } from './trait.service';

@Component({
    selector: 'jhi-trait-delete-dialog',
    templateUrl: './trait-delete-dialog.component.html'
})
export class TraitDeleteDialogComponent {
    trait: ITrait;

    constructor(protected traitService: TraitService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.traitService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'traitListModification',
                content: 'Deleted an trait'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trait-delete-popup',
    template: ''
})
export class TraitDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ trait }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(TraitDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.trait = trait;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/trait', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/trait', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
