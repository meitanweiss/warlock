import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ITrait } from 'app/shared/model/trait.model';
import { TraitService } from './trait.service';
import { IBackground } from 'app/shared/model/background.model';
import { BackgroundService } from 'app/entities/background';

@Component({
    selector: 'jhi-trait-update',
    templateUrl: './trait-update.component.html'
})
export class TraitUpdateComponent implements OnInit {
    trait: ITrait;
    isSaving: boolean;

    backgrounds: IBackground[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected traitService: TraitService,
        protected backgroundService: BackgroundService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ trait }) => {
            this.trait = trait;
        });
        this.backgroundService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBackground[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBackground[]>) => response.body)
            )
            .subscribe((res: IBackground[]) => (this.backgrounds = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.trait.id !== undefined) {
            this.subscribeToSaveResponse(this.traitService.update(this.trait));
        } else {
            this.subscribeToSaveResponse(this.traitService.create(this.trait));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrait>>) {
        result.subscribe((res: HttpResponse<ITrait>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBackgroundById(index: number, item: IBackground) {
        return item.id;
    }
}
