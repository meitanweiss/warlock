import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITrait } from 'app/shared/model/trait.model';

@Component({
    selector: 'jhi-trait-detail',
    templateUrl: './trait-detail.component.html'
})
export class TraitDetailComponent implements OnInit {
    trait: ITrait;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ trait }) => {
            this.trait = trait;
        });
    }

    previousState() {
        window.history.back();
    }
}
