import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'race',
                loadChildren: './race/race.module#WarlockRaceModule'
            },
            {
                path: 'race',
                loadChildren: './race/race.module#WarlockRaceModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'race',
                loadChildren: './race/race.module#WarlockRaceModule'
            },
            {
                path: 'character-class',
                loadChildren: './character-class/character-class.module#WarlockCharacterClassModule'
            },
            {
                path: 'background',
                loadChildren: './background/background.module#WarlockBackgroundModule'
            },
            {
                path: 'bonus',
                loadChildren: './bonus/bonus.module#WarlockBonusModule'
            },
            {
                path: 'ability',
                loadChildren: './ability/ability.module#WarlockAbilityModule'
            },
            {
                path: 'proficiency',
                loadChildren: './proficiency/proficiency.module#WarlockProficiencyModule'
            },
            {
                path: 'stats',
                loadChildren: './stats/stats.module#WarlockStatsModule'
            },
            {
                path: 'chara',
                loadChildren: './chara/chara.module#WarlockCharaModule'
            },
            {
                path: 'background',
                loadChildren: './background/background.module#WarlockBackgroundModule'
            },
            {
                path: 'bonus',
                loadChildren: './bonus/bonus.module#WarlockBonusModule'
            },
            {
                path: 'proficiency',
                loadChildren: './proficiency/proficiency.module#WarlockProficiencyModule'
            },
            {
                path: 'stats',
                loadChildren: './stats/stats.module#WarlockStatsModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'chara',
                loadChildren: './chara/chara.module#WarlockCharaModule'
            },
            {
                path: 'chara',
                loadChildren: './chara/chara.module#WarlockCharaModule'
            },
            {
                path: 'race',
                loadChildren: './race/race.module#WarlockRaceModule'
            },
            {
                path: 'character-class',
                loadChildren: './character-class/character-class.module#WarlockCharacterClassModule'
            },
            {
                path: 'ability',
                loadChildren: './ability/ability.module#WarlockAbilityModule'
            },
            {
                path: 'character-class',
                loadChildren: './character-class/character-class.module#WarlockCharacterClassModule'
            },
            {
                path: 'race',
                loadChildren: './race/race.module#WarlockRaceModule'
            },
            {
                path: 'race',
                loadChildren: './race/race.module#WarlockRaceModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'bonus',
                loadChildren: './bonus/bonus.module#WarlockBonusModule'
            },
            {
                path: 'ability',
                loadChildren: './ability/ability.module#WarlockAbilityModule'
            },
            {
                path: 'proficiency',
                loadChildren: './proficiency/proficiency.module#WarlockProficiencyModule'
            },
            {
                path: 'background',
                loadChildren: './background/background.module#WarlockBackgroundModule'
            },
            {
                path: 'background',
                loadChildren: './background/background.module#WarlockBackgroundModule'
            },
            {
                path: 'proficiency',
                loadChildren: './proficiency/proficiency.module#WarlockProficiencyModule'
            },
            {
                path: 'ability',
                loadChildren: './ability/ability.module#WarlockAbilityModule'
            },
            {
                path: 'proficiency',
                loadChildren: './proficiency/proficiency.module#WarlockProficiencyModule'
            },
            {
                path: 'bonus',
                loadChildren: './bonus/bonus.module#WarlockBonusModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'racial-traits',
                loadChildren: './racial-traits/racial-traits.module#WarlockRacialTraitsModule'
            },
            {
                path: 'bonus',
                loadChildren: './bonus/bonus.module#WarlockBonusModule'
            },
            {
                path: 'user-profile',
                loadChildren: './user-profile/user-profile.module#WarlockUserProfileModule'
            },
            {
                path: 'user-profile',
                loadChildren: './user-profile/user-profile.module#WarlockUserProfileModule'
            },
            {
                path: 'chara',
                loadChildren: './chara/chara.module#WarlockCharaModule'
            }
            /* jhipster// needle-add-entity-route -  entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockEntityModule {}
