import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Proficiency } from 'app/shared/model/proficiency.model';
import { ProficiencyService } from './proficiency.service';
import { ProficiencyComponent } from './proficiency.component';
import { ProficiencyDetailComponent } from './proficiency-detail.component';
import { ProficiencyUpdateComponent } from './proficiency-update.component';
import { ProficiencyDeletePopupComponent } from './proficiency-delete-dialog.component';
import { IProficiency } from 'app/shared/model/proficiency.model';

@Injectable({ providedIn: 'root' })
export class ProficiencyResolve implements Resolve<IProficiency> {
    constructor(private service: ProficiencyService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IProficiency> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Proficiency>) => response.ok),
                map((proficiency: HttpResponse<Proficiency>) => proficiency.body)
            );
        }
        return of(new Proficiency());
    }
}

export const proficiencyRoute: Routes = [
    {
        path: '',
        component: ProficiencyComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.proficiency.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ProficiencyDetailComponent,
        resolve: {
            proficiency: ProficiencyResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.proficiency.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ProficiencyUpdateComponent,
        resolve: {
            proficiency: ProficiencyResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.proficiency.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ProficiencyUpdateComponent,
        resolve: {
            proficiency: ProficiencyResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.proficiency.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const proficiencyPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ProficiencyDeletePopupComponent,
        resolve: {
            proficiency: ProficiencyResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.proficiency.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
