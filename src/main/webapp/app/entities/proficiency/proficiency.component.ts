import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IProficiency } from 'app/shared/model/proficiency.model';
import { AccountService } from 'app/core';
import { ProficiencyService } from './proficiency.service';

@Component({
    selector: 'my-proficiency',
    templateUrl: './proficiency.component.html'
})
export class ProficiencyComponent implements OnInit, OnDestroy {
    proficiencies: IProficiency[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected proficiencyService: ProficiencyService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.proficiencyService
            .query()
            .pipe(
                filter((res: HttpResponse<IProficiency[]>) => res.ok),
                map((res: HttpResponse<IProficiency[]>) => res.body)
            )
            .subscribe(
                (res: IProficiency[]) => {
                    this.proficiencies = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProficiencies();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProficiency) {
        return item.id;
    }

    registerChangeInProficiencies() {
        this.eventSubscriber = this.eventManager.subscribe('proficiencyListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
