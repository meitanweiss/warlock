import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProficiency } from 'app/shared/model/proficiency.model';

@Component({
    selector: 'my-proficiency-detail',
    templateUrl: './proficiency-detail.component.html'
})
export class ProficiencyDetailComponent implements OnInit {
    proficiency: IProficiency;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ proficiency }) => {
            this.proficiency = proficiency;
        });
    }

    previousState() {
        window.history.back();
    }
}
