import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    ProficiencyComponent,
    ProficiencyDetailComponent,
    ProficiencyUpdateComponent,
    ProficiencyDeletePopupComponent,
    ProficiencyDeleteDialogComponent,
    proficiencyRoute,
    proficiencyPopupRoute
} from './';

const ENTITY_STATES = [...proficiencyRoute, ...proficiencyPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProficiencyComponent,
        ProficiencyDetailComponent,
        ProficiencyUpdateComponent,
        ProficiencyDeleteDialogComponent,
        ProficiencyDeletePopupComponent
    ],
    entryComponents: [ProficiencyComponent, ProficiencyUpdateComponent, ProficiencyDeleteDialogComponent, ProficiencyDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockProficiencyModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
