import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProficiency } from 'app/shared/model/proficiency.model';
import { ProficiencyService } from './proficiency.service';

@Component({
    selector: 'my-proficiency-delete-dialog',
    templateUrl: './proficiency-delete-dialog.component.html'
})
export class ProficiencyDeleteDialogComponent {
    proficiency: IProficiency;

    constructor(
        protected proficiencyService: ProficiencyService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.proficiencyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'proficiencyListModification',
                content: 'Deleted an proficiency'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'my-proficiency-delete-popup',
    template: ''
})
export class ProficiencyDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ proficiency }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ProficiencyDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.proficiency = proficiency;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/proficiency', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/proficiency', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
