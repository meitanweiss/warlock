import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProficiency } from 'app/shared/model/proficiency.model';

type EntityResponseType = HttpResponse<IProficiency>;
type EntityArrayResponseType = HttpResponse<IProficiency[]>;

@Injectable({ providedIn: 'root' })
export class ProficiencyService {
    public resourceUrl = SERVER_API_URL + 'api/proficiencies';

    constructor(protected http: HttpClient) {}

    create(proficiency: IProficiency): Observable<EntityResponseType> {
        return this.http.post<IProficiency>(this.resourceUrl, proficiency, { observe: 'response' });
    }

    update(proficiency: IProficiency): Observable<EntityResponseType> {
        return this.http.put<IProficiency>(this.resourceUrl, proficiency, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IProficiency>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IProficiency[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
