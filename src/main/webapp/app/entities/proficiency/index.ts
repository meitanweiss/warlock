export * from './proficiency.service';
export * from './proficiency-update.component';
export * from './proficiency-delete-dialog.component';
export * from './proficiency-detail.component';
export * from './proficiency.component';
export * from './proficiency.route';
