import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IProficiency } from 'app/shared/model/proficiency.model';
import { ProficiencyService } from './proficiency.service';
import { IBackground } from 'app/shared/model/background.model';
import { BackgroundService } from 'app/entities/background';
import { ICharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from 'app/entities/character-class';

@Component({
    selector: 'my-proficiency-update',
    templateUrl: './proficiency-update.component.html'
})
export class ProficiencyUpdateComponent implements OnInit {
    proficiency: IProficiency;
    isSaving: boolean;

    backgrounds: IBackground[];

    characterclasses: ICharacterClass[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected proficiencyService: ProficiencyService,
        protected backgroundService: BackgroundService,
        protected characterClassService: CharacterClassService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ proficiency }) => {
            this.proficiency = proficiency;
        });
        this.backgroundService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBackground[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBackground[]>) => response.body)
            )
            .subscribe((res: IBackground[]) => (this.backgrounds = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.characterClassService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICharacterClass[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICharacterClass[]>) => response.body)
            )
            .subscribe((res: ICharacterClass[]) => (this.characterclasses = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.proficiency.id !== undefined) {
            this.subscribeToSaveResponse(this.proficiencyService.update(this.proficiency));
        } else {
            this.subscribeToSaveResponse(this.proficiencyService.create(this.proficiency));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IProficiency>>) {
        result.subscribe((res: HttpResponse<IProficiency>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBackgroundById(index: number, item: IBackground) {
        return item.id;
    }

    trackCharacterClassById(index: number, item: ICharacterClass) {
        return item.id;
    }
}
