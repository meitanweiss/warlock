import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    RaceComponent,
    RaceDetailComponent,
    RaceUpdateComponent,
    RaceDeletePopupComponent,
    RaceDeleteDialogComponent,
    raceRoute,
    racePopupRoute
} from './';

const ENTITY_STATES = [...raceRoute, ...racePopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [RaceComponent, RaceDetailComponent, RaceUpdateComponent, RaceDeleteDialogComponent, RaceDeletePopupComponent],
    entryComponents: [RaceComponent, RaceUpdateComponent, RaceDeleteDialogComponent, RaceDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockRaceModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
