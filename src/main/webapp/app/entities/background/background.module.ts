import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    BackgroundComponent,
    BackgroundDetailComponent,
    BackgroundUpdateComponent,
    BackgroundDeletePopupComponent,
    BackgroundDeleteDialogComponent,
    backgroundRoute,
    backgroundPopupRoute
} from './';

const ENTITY_STATES = [...backgroundRoute, ...backgroundPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        BackgroundComponent,
        BackgroundDetailComponent,
        BackgroundUpdateComponent,
        BackgroundDeleteDialogComponent,
        BackgroundDeletePopupComponent
    ],
    entryComponents: [BackgroundComponent, BackgroundUpdateComponent, BackgroundDeleteDialogComponent, BackgroundDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockBackgroundModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
