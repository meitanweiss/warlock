import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Background } from 'app/shared/model/background.model';
import { BackgroundService } from './background.service';
import { BackgroundComponent } from './background.component';
import { BackgroundDetailComponent } from './background-detail.component';
import { BackgroundUpdateComponent } from './background-update.component';
import { BackgroundDeletePopupComponent } from './background-delete-dialog.component';
import { IBackground } from 'app/shared/model/background.model';

@Injectable({ providedIn: 'root' })
export class BackgroundResolve implements Resolve<IBackground> {
    constructor(private service: BackgroundService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBackground> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Background>) => response.ok),
                map((background: HttpResponse<Background>) => background.body)
            );
        }
        return of(new Background());
    }
}

export const backgroundRoute: Routes = [
    {
        path: '',
        component: BackgroundComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            defaultSort: 'id,asc',
            pageTitle: 'warlockApp.background.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: BackgroundDetailComponent,
        resolve: {
            background: BackgroundResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.background.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: BackgroundUpdateComponent,
        resolve: {
            background: BackgroundResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.background.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: BackgroundUpdateComponent,
        resolve: {
            background: BackgroundResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.background.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const backgroundPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: BackgroundDeletePopupComponent,
        resolve: {
            background: BackgroundResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.background.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
