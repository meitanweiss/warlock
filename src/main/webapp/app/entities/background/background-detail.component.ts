import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBackground } from 'app/shared/model/background.model';

@Component({
    selector: 'jhi-background-detail',
    templateUrl: './background-detail.component.html'
})
export class BackgroundDetailComponent implements OnInit {
    background: IBackground;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ background }) => {
            this.background = background;
        });
    }

    previousState() {
        window.history.back();
    }
}
