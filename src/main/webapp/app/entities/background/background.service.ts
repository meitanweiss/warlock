import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBackground } from 'app/shared/model/background.model';

type EntityResponseType = HttpResponse<IBackground>;
type EntityArrayResponseType = HttpResponse<IBackground[]>;

@Injectable({ providedIn: 'root' })
export class BackgroundService {
    public resourceUrl = SERVER_API_URL + 'api/backgrounds';

    constructor(protected http: HttpClient) {}

    create(background: IBackground): Observable<EntityResponseType> {
        return this.http.post<IBackground>(this.resourceUrl, background, { observe: 'response' });
    }

    update(background: IBackground): Observable<EntityResponseType> {
        return this.http.put<IBackground>(this.resourceUrl, background, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IBackground>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IBackground[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
