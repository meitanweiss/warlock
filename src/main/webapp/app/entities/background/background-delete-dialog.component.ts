import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBackground } from 'app/shared/model/background.model';
import { BackgroundService } from './background.service';

@Component({
    selector: 'jhi-background-delete-dialog',
    templateUrl: './background-delete-dialog.component.html'
})
export class BackgroundDeleteDialogComponent {
    background: IBackground;

    constructor(
        protected backgroundService: BackgroundService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.backgroundService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'backgroundListModification',
                content: 'Deleted an background'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-background-delete-popup',
    template: ''
})
export class BackgroundDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ background }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(BackgroundDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.background = background;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/background', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/background', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
