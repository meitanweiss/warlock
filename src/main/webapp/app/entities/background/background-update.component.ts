import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IBackground } from 'app/shared/model/background.model';
import { BackgroundService } from './background.service';

@Component({
    selector: 'jhi-background-update',
    templateUrl: './background-update.component.html'
})
export class BackgroundUpdateComponent implements OnInit {
    background: IBackground;
    isSaving: boolean;

    constructor(protected backgroundService: BackgroundService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ background }) => {
            this.background = background;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.background.id !== undefined) {
            this.subscribeToSaveResponse(this.backgroundService.update(this.background));
        } else {
            this.subscribeToSaveResponse(this.backgroundService.create(this.background));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBackground>>) {
        result.subscribe((res: HttpResponse<IBackground>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
