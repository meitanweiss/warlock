import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    RacialTraitsComponent,
    RacialTraitsDetailComponent,
    RacialTraitsUpdateComponent,
    RacialTraitsDeletePopupComponent,
    RacialTraitsDeleteDialogComponent,
    racialTraitsRoute,
    racialTraitsPopupRoute
} from './';

const ENTITY_STATES = [...racialTraitsRoute, ...racialTraitsPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RacialTraitsComponent,
        RacialTraitsDetailComponent,
        RacialTraitsUpdateComponent,
        RacialTraitsDeleteDialogComponent,
        RacialTraitsDeletePopupComponent
    ],
    entryComponents: [
        RacialTraitsComponent,
        RacialTraitsUpdateComponent,
        RacialTraitsDeleteDialogComponent,
        RacialTraitsDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockRacialTraitsModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
