import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRacialTraits } from 'app/shared/model/racial-traits.model';

type EntityResponseType = HttpResponse<IRacialTraits>;
type EntityArrayResponseType = HttpResponse<IRacialTraits[]>;

@Injectable({ providedIn: 'root' })
export class RacialTraitsService {
    public resourceUrl = SERVER_API_URL + 'api/racial-traits';

    constructor(protected http: HttpClient) {}

    create(racialTraits: IRacialTraits): Observable<EntityResponseType> {
        return this.http.post<IRacialTraits>(this.resourceUrl, racialTraits, { observe: 'response' });
    }

    update(racialTraits: IRacialTraits): Observable<EntityResponseType> {
        return this.http.put<IRacialTraits>(this.resourceUrl, racialTraits, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRacialTraits>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRacialTraits[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
