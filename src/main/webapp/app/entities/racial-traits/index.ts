export * from './racial-traits.service';
export * from './racial-traits-update.component';
export * from './racial-traits-delete-dialog.component';
export * from './racial-traits-detail.component';
export * from './racial-traits.component';
export * from './racial-traits.route';
