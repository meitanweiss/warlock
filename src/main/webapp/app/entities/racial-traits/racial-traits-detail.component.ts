import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRacialTraits } from 'app/shared/model/racial-traits.model';

@Component({
    selector: 'my-racial-traits-detail',
    templateUrl: './racial-traits-detail.component.html'
})
export class RacialTraitsDetailComponent implements OnInit {
    racialTraits: IRacialTraits;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ racialTraits }) => {
            this.racialTraits = racialTraits;
        });
    }

    previousState() {
        window.history.back();
    }
}
