import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRacialTraits } from 'app/shared/model/racial-traits.model';
import { RacialTraitsService } from './racial-traits.service';
import { IRace } from 'app/shared/model/race.model';
import { RaceService } from 'app/entities/race';

@Component({
    selector: 'my-racial-traits-update',
    templateUrl: './racial-traits-update.component.html'
})
export class RacialTraitsUpdateComponent implements OnInit {
    racialTraits: IRacialTraits;
    isSaving: boolean;

    races: IRace[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected racialTraitsService: RacialTraitsService,
        protected raceService: RaceService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ racialTraits }) => {
            this.racialTraits = racialTraits;
        });
        this.raceService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IRace[]>) => mayBeOk.ok),
                map((response: HttpResponse<IRace[]>) => response.body)
            )
            .subscribe((res: IRace[]) => (this.races = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.racialTraits.id !== undefined) {
            this.subscribeToSaveResponse(this.racialTraitsService.update(this.racialTraits));
        } else {
            this.subscribeToSaveResponse(this.racialTraitsService.create(this.racialTraits));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IRacialTraits>>) {
        result.subscribe((res: HttpResponse<IRacialTraits>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRaceById(index: number, item: IRace) {
        return item.id;
    }
}
