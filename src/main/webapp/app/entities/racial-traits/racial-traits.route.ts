import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { RacialTraits } from 'app/shared/model/racial-traits.model';
import { RacialTraitsService } from './racial-traits.service';
import { RacialTraitsComponent } from './racial-traits.component';
import { RacialTraitsDetailComponent } from './racial-traits-detail.component';
import { RacialTraitsUpdateComponent } from './racial-traits-update.component';
import { RacialTraitsDeletePopupComponent } from './racial-traits-delete-dialog.component';
import { IRacialTraits } from 'app/shared/model/racial-traits.model';

@Injectable({ providedIn: 'root' })
export class RacialTraitsResolve implements Resolve<IRacialTraits> {
    constructor(private service: RacialTraitsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRacialTraits> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<RacialTraits>) => response.ok),
                map((racialTraits: HttpResponse<RacialTraits>) => racialTraits.body)
            );
        }
        return of(new RacialTraits());
    }
}

export const racialTraitsRoute: Routes = [
    {
        path: '',
        component: RacialTraitsComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            defaultSort: 'id,asc',
            pageTitle: 'warlockApp.racialTraits.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: RacialTraitsDetailComponent,
        resolve: {
            racialTraits: RacialTraitsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.racialTraits.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: RacialTraitsUpdateComponent,
        resolve: {
            racialTraits: RacialTraitsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.racialTraits.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: RacialTraitsUpdateComponent,
        resolve: {
            racialTraits: RacialTraitsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.racialTraits.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const racialTraitsPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: RacialTraitsDeletePopupComponent,
        resolve: {
            racialTraits: RacialTraitsResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.racialTraits.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
