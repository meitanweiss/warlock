import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRacialTraits } from 'app/shared/model/racial-traits.model';
import { RacialTraitsService } from './racial-traits.service';

@Component({
    selector: 'my-racial-traits-delete-dialog',
    templateUrl: './racial-traits-delete-dialog.component.html'
})
export class RacialTraitsDeleteDialogComponent {
    racialTraits: IRacialTraits;

    constructor(
        protected racialTraitsService: RacialTraitsService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.racialTraitsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'racialTraitsListModification',
                content: 'Deleted an racialTraits'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'my-racial-traits-delete-popup',
    template: ''
})
export class RacialTraitsDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ racialTraits }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(RacialTraitsDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.racialTraits = racialTraits;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/racial-traits', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/racial-traits', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
