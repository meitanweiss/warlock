import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';
import { BackgroundCharacteristicService } from './background-characteristic.service';
import { BackgroundCharacteristicComponent } from './background-characteristic.component';
import { BackgroundCharacteristicDetailComponent } from './background-characteristic-detail.component';
import { BackgroundCharacteristicUpdateComponent } from './background-characteristic-update.component';
import { BackgroundCharacteristicDeletePopupComponent } from './background-characteristic-delete-dialog.component';
import { IBackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';

@Injectable({ providedIn: 'root' })
export class BackgroundCharacteristicResolve implements Resolve<IBackgroundCharacteristic> {
    constructor(private service: BackgroundCharacteristicService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBackgroundCharacteristic> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<BackgroundCharacteristic>) => response.ok),
                map((backgroundCharacteristic: HttpResponse<BackgroundCharacteristic>) => backgroundCharacteristic.body)
            );
        }
        return of(new BackgroundCharacteristic());
    }
}

export const backgroundCharacteristicRoute: Routes = [
    {
        path: '',
        component: BackgroundCharacteristicComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            defaultSort: 'id,asc',
            pageTitle: 'warlockApp.backgroundCharacteristic.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: BackgroundCharacteristicDetailComponent,
        resolve: {
            backgroundCharacteristic: BackgroundCharacteristicResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.backgroundCharacteristic.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: BackgroundCharacteristicUpdateComponent,
        resolve: {
            backgroundCharacteristic: BackgroundCharacteristicResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.backgroundCharacteristic.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: BackgroundCharacteristicUpdateComponent,
        resolve: {
            backgroundCharacteristic: BackgroundCharacteristicResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.backgroundCharacteristic.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const backgroundCharacteristicPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: BackgroundCharacteristicDeletePopupComponent,
        resolve: {
            backgroundCharacteristic: BackgroundCharacteristicResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.backgroundCharacteristic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
