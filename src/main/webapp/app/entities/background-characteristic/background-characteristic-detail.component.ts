import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';

@Component({
    selector: 'jhi-background-characteristic-detail',
    templateUrl: './background-characteristic-detail.component.html'
})
export class BackgroundCharacteristicDetailComponent implements OnInit {
    backgroundCharacteristic: IBackgroundCharacteristic;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ backgroundCharacteristic }) => {
            this.backgroundCharacteristic = backgroundCharacteristic;
        });
    }

    previousState() {
        window.history.back();
    }
}
