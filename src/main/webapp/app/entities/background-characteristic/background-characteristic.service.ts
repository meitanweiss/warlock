import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';

type EntityResponseType = HttpResponse<IBackgroundCharacteristic>;
type EntityArrayResponseType = HttpResponse<IBackgroundCharacteristic[]>;

@Injectable({ providedIn: 'root' })
export class BackgroundCharacteristicService {
    public resourceUrl = SERVER_API_URL + 'api/background-characteristics';

    constructor(protected http: HttpClient) {}

    create(backgroundCharacteristic: IBackgroundCharacteristic): Observable<EntityResponseType> {
        return this.http.post<IBackgroundCharacteristic>(this.resourceUrl, backgroundCharacteristic, { observe: 'response' });
    }

    update(backgroundCharacteristic: IBackgroundCharacteristic): Observable<EntityResponseType> {
        return this.http.put<IBackgroundCharacteristic>(this.resourceUrl, backgroundCharacteristic, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IBackgroundCharacteristic>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IBackgroundCharacteristic[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
