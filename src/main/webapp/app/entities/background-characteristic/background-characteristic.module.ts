import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    BackgroundCharacteristicComponent,
    BackgroundCharacteristicDetailComponent,
    BackgroundCharacteristicUpdateComponent,
    BackgroundCharacteristicDeletePopupComponent,
    BackgroundCharacteristicDeleteDialogComponent,
    backgroundCharacteristicRoute,
    backgroundCharacteristicPopupRoute
} from './';

const ENTITY_STATES = [...backgroundCharacteristicRoute, ...backgroundCharacteristicPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        BackgroundCharacteristicComponent,
        BackgroundCharacteristicDetailComponent,
        BackgroundCharacteristicUpdateComponent,
        BackgroundCharacteristicDeleteDialogComponent,
        BackgroundCharacteristicDeletePopupComponent
    ],
    entryComponents: [
        BackgroundCharacteristicComponent,
        BackgroundCharacteristicUpdateComponent,
        BackgroundCharacteristicDeleteDialogComponent,
        BackgroundCharacteristicDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockBackgroundCharacteristicModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
