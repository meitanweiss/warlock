import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';
import { BackgroundCharacteristicService } from './background-characteristic.service';

@Component({
    selector: 'jhi-background-characteristic-delete-dialog',
    templateUrl: './background-characteristic-delete-dialog.component.html'
})
export class BackgroundCharacteristicDeleteDialogComponent {
    backgroundCharacteristic: IBackgroundCharacteristic;

    constructor(
        protected backgroundCharacteristicService: BackgroundCharacteristicService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.backgroundCharacteristicService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'backgroundCharacteristicListModification',
                content: 'Deleted an backgroundCharacteristic'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-background-characteristic-delete-popup',
    template: ''
})
export class BackgroundCharacteristicDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ backgroundCharacteristic }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(BackgroundCharacteristicDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.backgroundCharacteristic = backgroundCharacteristic;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/background-characteristic', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/background-characteristic', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
