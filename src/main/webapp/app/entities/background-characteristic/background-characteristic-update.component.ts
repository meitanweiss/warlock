import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';
import { BackgroundCharacteristicService } from './background-characteristic.service';
import { IBackground } from 'app/shared/model/background.model';
import { BackgroundService } from 'app/entities/background';

@Component({
    selector: 'jhi-background-characteristic-update',
    templateUrl: './background-characteristic-update.component.html'
})
export class BackgroundCharacteristicUpdateComponent implements OnInit {
    backgroundCharacteristic: IBackgroundCharacteristic;
    isSaving: boolean;

    backgrounds: IBackground[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected backgroundCharacteristicService: BackgroundCharacteristicService,
        protected backgroundService: BackgroundService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ backgroundCharacteristic }) => {
            this.backgroundCharacteristic = backgroundCharacteristic;
        });
        this.backgroundService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBackground[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBackground[]>) => response.body)
            )
            .subscribe((res: IBackground[]) => (this.backgrounds = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.backgroundCharacteristic.id !== undefined) {
            this.subscribeToSaveResponse(this.backgroundCharacteristicService.update(this.backgroundCharacteristic));
        } else {
            this.subscribeToSaveResponse(this.backgroundCharacteristicService.create(this.backgroundCharacteristic));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBackgroundCharacteristic>>) {
        result.subscribe(
            (res: HttpResponse<IBackgroundCharacteristic>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBackgroundById(index: number, item: IBackground) {
        return item.id;
    }
}
