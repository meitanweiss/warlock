import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAbility } from 'app/shared/model/ability.model';
import { AbilityService } from './ability.service';
import { ICharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from 'app/entities/character-class';

@Component({
    selector: 'my-ability-update',
    templateUrl: './ability-update.component.html'
})
export class AbilityUpdateComponent implements OnInit {
    ability: IAbility;
    isSaving: boolean;

    characterclasses: ICharacterClass[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected abilityService: AbilityService,
        protected characterClassService: CharacterClassService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ ability }) => {
            this.ability = ability;
        });
        this.characterClassService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICharacterClass[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICharacterClass[]>) => response.body)
            )
            .subscribe((res: ICharacterClass[]) => (this.characterclasses = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.ability.id !== undefined) {
            this.subscribeToSaveResponse(this.abilityService.update(this.ability));
        } else {
            this.subscribeToSaveResponse(this.abilityService.create(this.ability));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAbility>>) {
        result.subscribe((res: HttpResponse<IAbility>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCharacterClassById(index: number, item: ICharacterClass) {
        return item.id;
    }
}
