import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from './character-class.service';
import { CharacterClassComponent } from './character-class.component';
import { CharacterClassDetailComponent } from './character-class-detail.component';
import { CharacterClassUpdateComponent } from './character-class-update.component';
import { CharacterClassDeletePopupComponent } from './character-class-delete-dialog.component';
import { ICharacterClass } from 'app/shared/model/character-class.model';

@Injectable({ providedIn: 'root' })
export class CharacterClassResolve implements Resolve<ICharacterClass> {
    constructor(private service: CharacterClassService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICharacterClass> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CharacterClass>) => response.ok),
                map((characterClass: HttpResponse<CharacterClass>) => characterClass.body)
            );
        }
        return of(new CharacterClass());
    }
}

export const characterClassRoute: Routes = [
    {
        path: '',
        component: CharacterClassComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            defaultSort: 'id,asc',
            pageTitle: 'warlockApp.characterClass.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CharacterClassDetailComponent,
        resolve: {
            characterClass: CharacterClassResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'warlockApp.characterClass.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CharacterClassUpdateComponent,
        resolve: {
            characterClass: CharacterClassResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.characterClass.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CharacterClassUpdateComponent,
        resolve: {
            characterClass: CharacterClassResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.characterClass.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const characterClassPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CharacterClassDeletePopupComponent,
        resolve: {
            characterClass: CharacterClassResolve
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'warlockApp.characterClass.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
