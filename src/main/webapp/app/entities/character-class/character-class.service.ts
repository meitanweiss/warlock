import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICharacterClass } from 'app/shared/model/character-class.model';

type EntityResponseType = HttpResponse<ICharacterClass>;
type EntityArrayResponseType = HttpResponse<ICharacterClass[]>;

@Injectable({ providedIn: 'root' })
export class CharacterClassService {
    public resourceUrl = SERVER_API_URL + 'api/character-classes';

    constructor(protected http: HttpClient) {}

    create(characterClass: ICharacterClass): Observable<EntityResponseType> {
        return this.http.post<ICharacterClass>(this.resourceUrl, characterClass, { observe: 'response' });
    }

    update(characterClass: ICharacterClass): Observable<EntityResponseType> {
        return this.http.put<ICharacterClass>(this.resourceUrl, characterClass, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICharacterClass>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICharacterClass[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
