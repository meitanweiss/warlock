import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    CharacterClassComponent,
    CharacterClassDetailComponent,
    CharacterClassUpdateComponent,
    CharacterClassDeletePopupComponent,
    CharacterClassDeleteDialogComponent,
    characterClassRoute,
    characterClassPopupRoute
} from './';

const ENTITY_STATES = [...characterClassRoute, ...characterClassPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CharacterClassComponent,
        CharacterClassDetailComponent,
        CharacterClassUpdateComponent,
        CharacterClassDeleteDialogComponent,
        CharacterClassDeletePopupComponent
    ],
    entryComponents: [
        CharacterClassComponent,
        CharacterClassUpdateComponent,
        CharacterClassDeleteDialogComponent,
        CharacterClassDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockCharacterClassModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
