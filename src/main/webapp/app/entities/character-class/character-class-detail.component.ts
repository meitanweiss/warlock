import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICharacterClass } from 'app/shared/model/character-class.model';

@Component({
    selector: 'jhi-character-class-detail',
    templateUrl: './character-class-detail.component.html'
})
export class CharacterClassDetailComponent implements OnInit {
    characterClass: ICharacterClass;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ characterClass }) => {
            this.characterClass = characterClass;
        });
    }

    previousState() {
        window.history.back();
    }
}
