import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from './character-class.service';

@Component({
    selector: 'jhi-character-class-delete-dialog',
    templateUrl: './character-class-delete-dialog.component.html'
})
export class CharacterClassDeleteDialogComponent {
    characterClass: ICharacterClass;

    constructor(
        protected characterClassService: CharacterClassService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.characterClassService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'characterClassListModification',
                content: 'Deleted an characterClass'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-character-class-delete-popup',
    template: ''
})
export class CharacterClassDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ characterClass }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CharacterClassDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.characterClass = characterClass;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/character-class', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/character-class', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
