import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ICharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from './character-class.service';

@Component({
    selector: 'jhi-character-class-update',
    templateUrl: './character-class-update.component.html'
})
export class CharacterClassUpdateComponent implements OnInit {
    characterClass: ICharacterClass;
    isSaving: boolean;

    constructor(protected characterClassService: CharacterClassService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ characterClass }) => {
            this.characterClass = characterClass;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.characterClass.id !== undefined) {
            this.subscribeToSaveResponse(this.characterClassService.update(this.characterClass));
        } else {
            this.subscribeToSaveResponse(this.characterClassService.create(this.characterClass));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICharacterClass>>) {
        result.subscribe((res: HttpResponse<ICharacterClass>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
