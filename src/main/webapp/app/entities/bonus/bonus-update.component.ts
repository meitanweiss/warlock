import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBonus } from 'app/shared/model/bonus.model';
import { BonusService } from './bonus.service';
import { ICharacterClass } from 'app/shared/model/character-class.model';
import { CharacterClassService } from 'app/entities/character-class';
import { IRacialTraits } from 'app/shared/model/racial-traits.model';
import { RacialTraitsService } from 'app/entities/racial-traits';

@Component({
    selector: 'my-bonus-update',
    templateUrl: './bonus-update.component.html'
})
export class BonusUpdateComponent implements OnInit {
    bonus: IBonus;
    isSaving: boolean;

    characterclasses: ICharacterClass[];

    racialtraits: IRacialTraits[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected bonusService: BonusService,
        protected characterClassService: CharacterClassService,
        protected racialTraitsService: RacialTraitsService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ bonus }) => {
            this.bonus = bonus;
        });
        this.characterClassService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICharacterClass[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICharacterClass[]>) => response.body)
            )
            .subscribe((res: ICharacterClass[]) => (this.characterclasses = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.racialTraitsService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IRacialTraits[]>) => mayBeOk.ok),
                map((response: HttpResponse<IRacialTraits[]>) => response.body)
            )
            .subscribe((res: IRacialTraits[]) => (this.racialtraits = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.bonus.id !== undefined) {
            this.subscribeToSaveResponse(this.bonusService.update(this.bonus));
        } else {
            this.subscribeToSaveResponse(this.bonusService.create(this.bonus));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBonus>>) {
        result.subscribe((res: HttpResponse<IBonus>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCharacterClassById(index: number, item: ICharacterClass) {
        return item.id;
    }

    trackRacialTraitsById(index: number, item: IRacialTraits) {
        return item.id;
    }
}
