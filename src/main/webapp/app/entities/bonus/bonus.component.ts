import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBonus } from 'app/shared/model/bonus.model';
import { AccountService } from 'app/core';
import { BonusService } from './bonus.service';

@Component({
    selector: 'my-bonus',
    templateUrl: './bonus.component.html'
})
export class BonusComponent implements OnInit, OnDestroy {
    bonuses: IBonus[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected bonusService: BonusService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.bonusService
            .query()
            .pipe(
                filter((res: HttpResponse<IBonus[]>) => res.ok),
                map((res: HttpResponse<IBonus[]>) => res.body)
            )
            .subscribe(
                (res: IBonus[]) => {
                    this.bonuses = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInBonuses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IBonus) {
        return item.id;
    }

    registerChangeInBonuses() {
        this.eventSubscriber = this.eventManager.subscribe('bonusListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
