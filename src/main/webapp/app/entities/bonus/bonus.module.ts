import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WarlockSharedModule } from 'app/shared';
import {
    BonusComponent,
    BonusDetailComponent,
    BonusUpdateComponent,
    BonusDeletePopupComponent,
    BonusDeleteDialogComponent,
    bonusRoute,
    bonusPopupRoute
} from './';

const ENTITY_STATES = [...bonusRoute, ...bonusPopupRoute];

@NgModule({
    imports: [WarlockSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [BonusComponent, BonusDetailComponent, BonusUpdateComponent, BonusDeleteDialogComponent, BonusDeletePopupComponent],
    entryComponents: [BonusComponent, BonusUpdateComponent, BonusDeleteDialogComponent, BonusDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WarlockBonusModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
