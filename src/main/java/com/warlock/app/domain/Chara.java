package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Chara.
 */
@Entity
@Table(name = "chara")
public class Chara implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "alingment")
    private String alingment;

    @Column(name = "faith")
    private String faith;

    @Column(name = "lifestyle")
    private String lifestyle;

    @Column(name = "hair")
    private String hair;

    @Column(name = "skin")
    private String skin;

    @Column(name = "eyes")
    private String eyes;

    @Column(name = "height")
    private Integer height;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "age")
    private Integer age;

    @Column(name = "flaws")
    private String flaws;

    @Column(name = "special")
    private String special;

    @Column(name = "gender")
    private String gender;

    @Column(name = "peronality")
    private String peronality;

    @Column(name = "ideals")
    private String ideals;

    @Column(name = "bonds")
    private String bonds;

    @OneToMany(mappedBy = "chara")
    private Set<Stats> stats = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("charas")
    private UserProfile userProfile;

    @ManyToOne
    @JsonIgnoreProperties("charas")
    private Background background;

    @ManyToOne
    @JsonIgnoreProperties("charas")
    private CharacterClass characterClass;

    @ManyToOne
    @JsonIgnoreProperties("charas")
    private Race race;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Chara name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlingment() {
        return alingment;
    }

    public Chara alingment(String alingment) {
        this.alingment = alingment;
        return this;
    }

    public void setAlingment(String alingment) {
        this.alingment = alingment;
    }

    public String getFaith() {
        return faith;
    }

    public Chara faith(String faith) {
        this.faith = faith;
        return this;
    }

    public void setFaith(String faith) {
        this.faith = faith;
    }

    public String getLifestyle() {
        return lifestyle;
    }

    public Chara lifestyle(String lifestyle) {
        this.lifestyle = lifestyle;
        return this;
    }

    public void setLifestyle(String lifestyle) {
        this.lifestyle = lifestyle;
    }

    public String getHair() {
        return hair;
    }

    public Chara hair(String hair) {
        this.hair = hair;
        return this;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public String getSkin() {
        return skin;
    }

    public Chara skin(String skin) {
        this.skin = skin;
        return this;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getEyes() {
        return eyes;
    }

    public Chara eyes(String eyes) {
        this.eyes = eyes;
        return this;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public Integer getHeight() {
        return height;
    }

    public Chara height(Integer height) {
        this.height = height;
        return this;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public Chara weight(Integer weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getAge() {
        return age;
    }

    public Chara age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getFlaws() {
        return flaws;
    }

    public Chara flaws(String flaws) {
        this.flaws = flaws;
        return this;
    }

    public void setFlaws(String flaws) {
        this.flaws = flaws;
    }

    public String getSpecial() {
        return special;
    }

    public Chara special(String special) {
        this.special = special;
        return this;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getGender() {
        return gender;
    }

    public Chara gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPeronality() {
        return peronality;
    }

    public Chara peronality(String peronality) {
        this.peronality = peronality;
        return this;
    }

    public void setPeronality(String peronality) {
        this.peronality = peronality;
    }

    public String getIdeals() {
        return ideals;
    }

    public Chara ideals(String ideals) {
        this.ideals = ideals;
        return this;
    }

    public void setIdeals(String ideals) {
        this.ideals = ideals;
    }

    public String getBonds() {
        return bonds;
    }

    public Chara bonds(String bonds) {
        this.bonds = bonds;
        return this;
    }

    public void setBonds(String bonds) {
        this.bonds = bonds;
    }

    public Set<Stats> getStats() {
        return stats;
    }

    public Chara stats(Set<Stats> stats) {
        this.stats = stats;
        return this;
    }

    public Chara addStats(Stats stats) {
        this.stats.add(stats);
        stats.setChara(this);
        return this;
    }

    public Chara removeStats(Stats stats) {
        this.stats.remove(stats);
        stats.setChara(null);
        return this;
    }

    public void setStats(Set<Stats> stats) {
        this.stats = stats;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public Chara userProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
        return this;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Background getBackground() {
        return background;
    }

    public Chara background(Background background) {
        this.background = background;
        return this;
    }

    public void setBackground(Background background) {
        this.background = background;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public Chara characterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
        return this;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public Race getRace() {
        return race;
    }

    public Chara race(Race race) {
        this.race = race;
        return this;
    }

    public void setRace(Race race) {
        this.race = race;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Chara chara = (Chara) o;
        if (chara.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chara.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Chara{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", alingment='" + getAlingment() + "'" +
            ", faith='" + getFaith() + "'" +
            ", lifestyle='" + getLifestyle() + "'" +
            ", hair='" + getHair() + "'" +
            ", skin='" + getSkin() + "'" +
            ", eyes='" + getEyes() + "'" +
            ", height=" + getHeight() +
            ", weight=" + getWeight() +
            ", age=" + getAge() +
            ", flaws='" + getFlaws() + "'" +
            ", special='" + getSpecial() + "'" +
            ", gender='" + getGender() + "'" +
            ", peronality='" + getPeronality() + "'" +
            ", ideals='" + getIdeals() + "'" +
            ", bonds='" + getBonds() + "'" +
            "}";
    }
}
