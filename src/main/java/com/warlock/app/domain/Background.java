package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Background.
 */
@Entity
@Table(name = "background")
public class Background implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "background")
    private Set<Proficiency> proficiencies = new HashSet<>();
    @OneToMany(mappedBy = "background")
    private Set<Chara> charas = new HashSet<>();
    @OneToMany(mappedBy = "background")
    private Set<Trait> traits = new HashSet<>();
    @OneToMany(mappedBy = "background")
    private Set<BackgroundCharacteristic> backgroundCharacteristics = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Background name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Background description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Proficiency> getProficiencies() {
        return proficiencies;
    }

    public Background proficiencies(Set<Proficiency> proficiencies) {
        this.proficiencies = proficiencies;
        return this;
    }

    public Background addProficiency(Proficiency proficiency) {
        this.proficiencies.add(proficiency);
        proficiency.setBackground(this);
        return this;
    }

    public Background removeProficiency(Proficiency proficiency) {
        this.proficiencies.remove(proficiency);
        proficiency.setBackground(null);
        return this;
    }

    public void setProficiencies(Set<Proficiency> proficiencies) {
        this.proficiencies = proficiencies;
    }

    public Set<Chara> getCharas() {
        return charas;
    }

    public Background charas(Set<Chara> charas) {
        this.charas = charas;
        return this;
    }

    public Background addChara(Chara chara) {
        this.charas.add(chara);
        chara.setBackground(this);
        return this;
    }

    public Background removeChara(Chara chara) {
        this.charas.remove(chara);
        chara.setBackground(null);
        return this;
    }

    public void setCharas(Set<Chara> charas) {
        this.charas = charas;
    }

    public Set<Trait> getTraits() {
        return traits;
    }

    public Background traits(Set<Trait> traits) {
        this.traits = traits;
        return this;
    }

    public Background addTrait(Trait trait) {
        this.traits.add(trait);
        trait.setBackground(this);
        return this;
    }

    public Background removeTrait(Trait trait) {
        this.traits.remove(trait);
        trait.setBackground(null);
        return this;
    }

    public void setTraits(Set<Trait> traits) {
        this.traits = traits;
    }

    public Set<BackgroundCharacteristic> getBackgroundCharacteristics() {
        return backgroundCharacteristics;
    }

    public Background backgroundCharacteristics(Set<BackgroundCharacteristic> backgroundCharacteristics) {
        this.backgroundCharacteristics = backgroundCharacteristics;
        return this;
    }

    public Background addBackgroundCharacteristic(BackgroundCharacteristic backgroundCharacteristic) {
        this.backgroundCharacteristics.add(backgroundCharacteristic);
        backgroundCharacteristic.setBackground(this);
        return this;
    }

    public Background removeBackgroundCharacteristic(BackgroundCharacteristic backgroundCharacteristic) {
        this.backgroundCharacteristics.remove(backgroundCharacteristic);
        backgroundCharacteristic.setBackground(null);
        return this;
    }

    public void setBackgroundCharacteristics(Set<BackgroundCharacteristic> backgroundCharacteristics) {
        this.backgroundCharacteristics = backgroundCharacteristics;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Background background = (Background) o;
        if (background.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), background.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Background{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
