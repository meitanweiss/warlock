package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Race.
 */
@Entity
@Table(name = "race")
public class Race implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "summary")
    private String summary;

    @Column(name = "description")
    private String description;

    @Column(name = "age")
    private String age;

    @Column(name = "alignment")
    private String alignment;

    @Column(name = "jhi_size")
    private String size;

    @Column(name = "speed")
    private String speed;

    @Column(name = "languages")
    private String languages;

    @OneToMany(mappedBy = "race")
    private Set<RacialTraits> racialTraits = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Race name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public Race summary(String summary) {
        this.summary = summary;
        return this;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public Race description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAge() {
        return age;
    }

    public Race age(String age) {
        this.age = age;
        return this;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAlignment() {
        return alignment;
    }

    public Race alignment(String alignment) {
        this.alignment = alignment;
        return this;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public String getSize() {
        return size;
    }

    public Race size(String size) {
        this.size = size;
        return this;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSpeed() {
        return speed;
    }

    public Race speed(String speed) {
        this.speed = speed;
        return this;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLanguages() {
        return languages;
    }

    public Race languages(String languages) {
        this.languages = languages;
        return this;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public Set<RacialTraits> getRacialTraits() {
        return racialTraits;
    }

    public Race racialTraits(Set<RacialTraits> racialTraits) {
        this.racialTraits = racialTraits;
        return this;
    }

    public Race addRacialTraits(RacialTraits racialTraits) {
        this.racialTraits.add(racialTraits);
        racialTraits.setRace(this);
        return this;
    }

    public Race removeRacialTraits(RacialTraits racialTraits) {
        this.racialTraits.remove(racialTraits);
        racialTraits.setRace(null);
        return this;
    }

    public void setRacialTraits(Set<RacialTraits> racialTraits) {
        this.racialTraits = racialTraits;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Race race = (Race) o;
        if (race.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), race.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Race{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", summary='" + getSummary() + "'" +
            ", description='" + getDescription() + "'" +
            ", age='" + getAge() + "'" +
            ", alignment='" + getAlignment() + "'" +
            ", size='" + getSize() + "'" +
            ", speed='" + getSpeed() + "'" +
            ", languages='" + getLanguages() + "'" +
            "}";
    }
}
