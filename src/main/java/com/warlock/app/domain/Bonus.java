package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Bonus.
 */
@Entity
@Table(name = "bonus")
public class Bonus implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "jhi_change")
    private Integer change;

    @Column(name = "stat")
    private String stat;

    @ManyToOne
    @JsonIgnoreProperties("bonuses")
    private CharacterClass characterClass;

    @ManyToOne
    @JsonIgnoreProperties("bonuses")
    private RacialTraits racialTraits;

    // needle-entity-add-field -  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Bonus name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getChange() {
        return change;
    }

    public Bonus change(Integer change) {
        this.change = change;
        return this;
    }

    public void setChange(Integer change) {
        this.change = change;
    }

    public String getStat() {
        return stat;
    }

    public Bonus stat(String stat) {
        this.stat = stat;
        return this;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public Bonus characterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
        return this;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public RacialTraits getRacialTraits() {
        return racialTraits;
    }

    public Bonus racialTraits(RacialTraits racialTraits) {
        this.racialTraits = racialTraits;
        return this;
    }

    public void setRacialTraits(RacialTraits racialTraits) {
        this.racialTraits = racialTraits;
    }
    // needle-entity-add-getters-setters -  getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bonus bonus = (Bonus) o;
        if (bonus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bonus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bonus{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", change=" + getChange() +
            ", stat='" + getStat() + "'" +
            "}";
    }
}
