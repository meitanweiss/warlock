package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A RacialTraits.
 */
@Entity
@Table(name = "racial_traits")
public class RacialTraits implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("racialTraits")
    private Race race;

    @OneToMany(mappedBy = "racialTraits")
    private Set<Bonus> bonuses = new HashSet<>();
    // needle-entity-add-field -  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public RacialTraits name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public RacialTraits description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Race getRace() {
        return race;
    }

    public RacialTraits race(Race race) {
        this.race = race;
        return this;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Set<Bonus> getBonuses() {
        return bonuses;
    }

    public RacialTraits bonuses(Set<Bonus> bonuses) {
        this.bonuses = bonuses;
        return this;
    }

    public RacialTraits addBonus(Bonus bonus) {
        this.bonuses.add(bonus);
        bonus.setRacialTraits(this);
        return this;
    }

    public RacialTraits removeBonus(Bonus bonus) {
        this.bonuses.remove(bonus);
        bonus.setRacialTraits(null);
        return this;
    }

    public void setBonuses(Set<Bonus> bonuses) {
        this.bonuses = bonuses;
    }
    // needle-entity-add-getters-setters -  getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RacialTraits racialTraits = (RacialTraits) o;
        if (racialTraits.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), racialTraits.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RacialTraits{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
