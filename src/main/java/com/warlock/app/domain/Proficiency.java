package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Proficiency.
 */
@Entity
@Table(name = "proficiency")
public class Proficiency implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("proficiencies")
    private Background background;

    @ManyToOne
    @JsonIgnoreProperties("proficiencies")
    private CharacterClass characterClass;

    // needle-entity-add-field -  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Proficiency name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Proficiency description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Background getBackground() {
        return background;
    }

    public Proficiency background(Background background) {
        this.background = background;
        return this;
    }

    public void setBackground(Background background) {
        this.background = background;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public Proficiency characterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
        return this;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }
    // needle-entity-add-getters-setters -  getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Proficiency proficiency = (Proficiency) o;
        if (proficiency.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), proficiency.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Proficiency{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
