package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Stats.
 */
@Entity
@Table(name = "stats")
public class Stats implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "num")
    private Integer num;

    @Column(name = "abbr")
    private String abbr;

    @ManyToOne
    @JsonIgnoreProperties("stats")
    private Chara chara;

    // needle-entity-add-field -  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Stats name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNum() {
        return num;
    }

    public Stats num(Integer num) {
        this.num = num;
        return this;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getAbbr() {
        return abbr;
    }

    public Stats abbr(String abbr) {
        this.abbr = abbr;
        return this;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public Chara getChara() {
        return chara;
    }

    public Stats chara(Chara chara) {
        this.chara = chara;
        return this;
    }

    public void setChara(Chara chara) {
        this.chara = chara;
    }
    // needle-entity-add-getters-setters -  getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Stats stats = (Stats) o;
        if (stats.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), stats.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Stats{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", num=" + getNum() +
            ", abbr='" + getAbbr() + "'" +
            "}";
    }
}
