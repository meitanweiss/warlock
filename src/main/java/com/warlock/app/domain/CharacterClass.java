package com.warlock.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CharacterClass.
 */
@Entity
@Table(name = "character_class")
public class CharacterClass implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "hp")
    private String hp;

    @OneToMany(mappedBy = "characterClass")
    private Set<Proficiency> proficiencies = new HashSet<>();
    @OneToMany(mappedBy = "characterClass")
    private Set<Ability> abilities = new HashSet<>();
    @OneToMany(mappedBy = "characterClass")
    private Set<Bonus> bonuses = new HashSet<>();
    @OneToMany(mappedBy = "characterClass")
    private Set<Chara> charas = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CharacterClass name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public CharacterClass description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHp() {
        return hp;
    }

    public CharacterClass hp(String hp) {
        this.hp = hp;
        return this;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public Set<Proficiency> getProficiencies() {
        return proficiencies;
    }

    public CharacterClass proficiencies(Set<Proficiency> proficiencies) {
        this.proficiencies = proficiencies;
        return this;
    }

    public CharacterClass addProficiency(Proficiency proficiency) {
        this.proficiencies.add(proficiency);
        proficiency.setCharacterClass(this);
        return this;
    }

    public CharacterClass removeProficiency(Proficiency proficiency) {
        this.proficiencies.remove(proficiency);
        proficiency.setCharacterClass(null);
        return this;
    }

    public void setProficiencies(Set<Proficiency> proficiencies) {
        this.proficiencies = proficiencies;
    }

    public Set<Ability> getAbilities() {
        return abilities;
    }

    public CharacterClass abilities(Set<Ability> abilities) {
        this.abilities = abilities;
        return this;
    }

    public CharacterClass addAbility(Ability ability) {
        this.abilities.add(ability);
        ability.setCharacterClass(this);
        return this;
    }

    public CharacterClass removeAbility(Ability ability) {
        this.abilities.remove(ability);
        ability.setCharacterClass(null);
        return this;
    }

    public void setAbilities(Set<Ability> abilities) {
        this.abilities = abilities;
    }

    public Set<Bonus> getBonuses() {
        return bonuses;
    }

    public CharacterClass bonuses(Set<Bonus> bonuses) {
        this.bonuses = bonuses;
        return this;
    }

    public CharacterClass addBonus(Bonus bonus) {
        this.bonuses.add(bonus);
        bonus.setCharacterClass(this);
        return this;
    }

    public CharacterClass removeBonus(Bonus bonus) {
        this.bonuses.remove(bonus);
        bonus.setCharacterClass(null);
        return this;
    }

    public void setBonuses(Set<Bonus> bonuses) {
        this.bonuses = bonuses;
    }

    public Set<Chara> getCharas() {
        return charas;
    }

    public CharacterClass charas(Set<Chara> charas) {
        this.charas = charas;
        return this;
    }

    public CharacterClass addChara(Chara chara) {
        this.charas.add(chara);
        chara.setCharacterClass(this);
        return this;
    }

    public CharacterClass removeChara(Chara chara) {
        this.charas.remove(chara);
        chara.setCharacterClass(null);
        return this;
    }

    public void setCharas(Set<Chara> charas) {
        this.charas = charas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CharacterClass characterClass = (CharacterClass) o;
        if (characterClass.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), characterClass.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CharacterClass{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", hp='" + getHp() + "'" +
            "}";
    }
}
