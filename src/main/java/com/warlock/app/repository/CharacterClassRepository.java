package com.warlock.app.repository;

import com.warlock.app.domain.CharacterClass;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CharacterClass entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CharacterClassRepository extends JpaRepository<CharacterClass, Long> {

}
