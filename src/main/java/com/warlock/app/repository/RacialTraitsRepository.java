package com.warlock.app.repository;

import com.warlock.app.domain.RacialTraits;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RacialTraits entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RacialTraitsRepository extends JpaRepository<RacialTraits, Long> {

}
