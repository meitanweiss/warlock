package com.warlock.app.repository;

import com.warlock.app.domain.Chara;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Chara entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CharaRepository extends JpaRepository<Chara, Long> {

}
