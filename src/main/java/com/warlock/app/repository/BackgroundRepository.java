package com.warlock.app.repository;

import com.warlock.app.domain.Background;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Background entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BackgroundRepository extends JpaRepository<Background, Long> {

}
