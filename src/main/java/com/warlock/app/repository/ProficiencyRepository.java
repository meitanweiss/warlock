package com.warlock.app.repository;

import com.warlock.app.domain.Proficiency;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Proficiency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProficiencyRepository extends JpaRepository<Proficiency, Long> {

}
