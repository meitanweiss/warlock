package com.warlock.app.repository;

import com.warlock.app.domain.BackgroundCharacteristic;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BackgroundCharacteristic entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BackgroundCharacteristicRepository extends JpaRepository<BackgroundCharacteristic, Long> {

}
