package com.warlock.app.repository;

import com.warlock.app.domain.Trait;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Trait entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraitRepository extends JpaRepository<Trait, Long> {

}
