/**
 * View Models used by Spring MVC REST controllers.
 */
package com.warlock.app.web.rest.vm;
