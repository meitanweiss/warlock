package com.warlock.app.web.rest;
import com.warlock.app.domain.RacialTraits;
import com.warlock.app.repository.RacialTraitsRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import com.warlock.app.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RacialTraits.
 */
@RestController
@RequestMapping("/api")
public class RacialTraitsResource {

    private final Logger log = LoggerFactory.getLogger(RacialTraitsResource.class);

    private static final String ENTITY_NAME = "racialTraits";

    private final RacialTraitsRepository racialTraitsRepository;

    public RacialTraitsResource(RacialTraitsRepository racialTraitsRepository) {
        this.racialTraitsRepository = racialTraitsRepository;
    }

    /**
     * POST  /racial-traits : Create a new racialTraits.
     *
     * @param racialTraits the racialTraits to create
     * @return the ResponseEntity with status 201 (Created) and with body the new racialTraits, or with status 400 (Bad Request) if the racialTraits has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/racial-traits")
    public ResponseEntity<RacialTraits> createRacialTraits(@RequestBody RacialTraits racialTraits) throws URISyntaxException {
        log.debug("REST request to save RacialTraits : {}", racialTraits);
        if (racialTraits.getId() != null) {
            throw new BadRequestAlertException("A new racialTraits cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RacialTraits result = racialTraitsRepository.save(racialTraits);
        return ResponseEntity.created(new URI("/api/racial-traits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /racial-traits : Updates an existing racialTraits.
     *
     * @param racialTraits the racialTraits to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated racialTraits,
     * or with status 400 (Bad Request) if the racialTraits is not valid,
     * or with status 500 (Internal Server Error) if the racialTraits couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/racial-traits")
    public ResponseEntity<RacialTraits> updateRacialTraits(@RequestBody RacialTraits racialTraits) throws URISyntaxException {
        log.debug("REST request to update RacialTraits : {}", racialTraits);
        if (racialTraits.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RacialTraits result = racialTraitsRepository.save(racialTraits);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, racialTraits.getId().toString()))
            .body(result);
    }

    /**
     * GET  /racial-traits : get all the racialTraits.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of racialTraits in body
     */
    @GetMapping("/racial-traits")
    public ResponseEntity<List<RacialTraits>> getAllRacialTraits(Pageable pageable) {
        log.debug("REST request to get a page of RacialTraits");
        Page<RacialTraits> page = racialTraitsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/racial-traits");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /racial-traits/:id : get the "id" racialTraits.
     *
     * @param id the id of the racialTraits to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the racialTraits, or with status 404 (Not Found)
     */
    @GetMapping("/racial-traits/{id}")
    public ResponseEntity<RacialTraits> getRacialTraits(@PathVariable Long id) {
        log.debug("REST request to get RacialTraits : {}", id);
        Optional<RacialTraits> racialTraits = racialTraitsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(racialTraits);
    }

    /**
     * DELETE  /racial-traits/:id : delete the "id" racialTraits.
     *
     * @param id the id of the racialTraits to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/racial-traits/{id}")
    public ResponseEntity<Void> deleteRacialTraits(@PathVariable Long id) {
        log.debug("REST request to delete RacialTraits : {}", id);
        racialTraitsRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
