package com.warlock.app.web.rest;
import com.warlock.app.domain.Proficiency;
import com.warlock.app.repository.ProficiencyRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Proficiency.
 */
@RestController
@RequestMapping("/api")
public class ProficiencyResource {

    private final Logger log = LoggerFactory.getLogger(ProficiencyResource.class);

    private static final String ENTITY_NAME = "proficiency";

    private final ProficiencyRepository proficiencyRepository;

    public ProficiencyResource(ProficiencyRepository proficiencyRepository) {
        this.proficiencyRepository = proficiencyRepository;
    }

    /**
     * POST  /proficiencies : Create a new proficiency.
     *
     * @param proficiency the proficiency to create
     * @return the ResponseEntity with status 201 (Created) and with body the new proficiency, or with status 400 (Bad Request) if the proficiency has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/proficiencies")
    public ResponseEntity<Proficiency> createProficiency(@RequestBody Proficiency proficiency) throws URISyntaxException {
        log.debug("REST request to save Proficiency : {}", proficiency);
        if (proficiency.getId() != null) {
            throw new BadRequestAlertException("A new proficiency cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Proficiency result = proficiencyRepository.save(proficiency);
        return ResponseEntity.created(new URI("/api/proficiencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /proficiencies : Updates an existing proficiency.
     *
     * @param proficiency the proficiency to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated proficiency,
     * or with status 400 (Bad Request) if the proficiency is not valid,
     * or with status 500 (Internal Server Error) if the proficiency couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/proficiencies")
    public ResponseEntity<Proficiency> updateProficiency(@RequestBody Proficiency proficiency) throws URISyntaxException {
        log.debug("REST request to update Proficiency : {}", proficiency);
        if (proficiency.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Proficiency result = proficiencyRepository.save(proficiency);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, proficiency.getId().toString()))
            .body(result);
    }

    /**
     * GET  /proficiencies : get all the proficiencies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of proficiencies in body
     */
    @GetMapping("/proficiencies")
    public List<Proficiency> getAllProficiencies() {
        log.debug("REST request to get all Proficiencies");
        return proficiencyRepository.findAll();
    }

    /**
     * GET  /proficiencies/:id : get the "id" proficiency.
     *
     * @param id the id of the proficiency to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the proficiency, or with status 404 (Not Found)
     */
    @GetMapping("/proficiencies/{id}")
    public ResponseEntity<Proficiency> getProficiency(@PathVariable Long id) {
        log.debug("REST request to get Proficiency : {}", id);
        Optional<Proficiency> proficiency = proficiencyRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(proficiency);
    }

    /**
     * DELETE  /proficiencies/:id : delete the "id" proficiency.
     *
     * @param id the id of the proficiency to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/proficiencies/{id}")
    public ResponseEntity<Void> deleteProficiency(@PathVariable Long id) {
        log.debug("REST request to delete Proficiency : {}", id);
        proficiencyRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
