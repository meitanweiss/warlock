package com.warlock.app.web.rest;
import com.warlock.app.domain.BackgroundCharacteristic;
import com.warlock.app.repository.BackgroundCharacteristicRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import com.warlock.app.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BackgroundCharacteristic.
 */
@RestController
@RequestMapping("/api")
public class BackgroundCharacteristicResource {

    private final Logger log = LoggerFactory.getLogger(BackgroundCharacteristicResource.class);

    private static final String ENTITY_NAME = "backgroundCharacteristic";

    private final BackgroundCharacteristicRepository backgroundCharacteristicRepository;

    public BackgroundCharacteristicResource(BackgroundCharacteristicRepository backgroundCharacteristicRepository) {
        this.backgroundCharacteristicRepository = backgroundCharacteristicRepository;
    }

    /**
     * POST  /background-characteristics : Create a new backgroundCharacteristic.
     *
     * @param backgroundCharacteristic the backgroundCharacteristic to create
     * @return the ResponseEntity with status 201 (Created) and with body the new backgroundCharacteristic, or with status 400 (Bad Request) if the backgroundCharacteristic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/background-characteristics")
    public ResponseEntity<BackgroundCharacteristic> createBackgroundCharacteristic(@RequestBody BackgroundCharacteristic backgroundCharacteristic) throws URISyntaxException {
        log.debug("REST request to save BackgroundCharacteristic : {}", backgroundCharacteristic);
        if (backgroundCharacteristic.getId() != null) {
            throw new BadRequestAlertException("A new backgroundCharacteristic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BackgroundCharacteristic result = backgroundCharacteristicRepository.save(backgroundCharacteristic);
        return ResponseEntity.created(new URI("/api/background-characteristics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /background-characteristics : Updates an existing backgroundCharacteristic.
     *
     * @param backgroundCharacteristic the backgroundCharacteristic to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated backgroundCharacteristic,
     * or with status 400 (Bad Request) if the backgroundCharacteristic is not valid,
     * or with status 500 (Internal Server Error) if the backgroundCharacteristic couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/background-characteristics")
    public ResponseEntity<BackgroundCharacteristic> updateBackgroundCharacteristic(@RequestBody BackgroundCharacteristic backgroundCharacteristic) throws URISyntaxException {
        log.debug("REST request to update BackgroundCharacteristic : {}", backgroundCharacteristic);
        if (backgroundCharacteristic.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BackgroundCharacteristic result = backgroundCharacteristicRepository.save(backgroundCharacteristic);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, backgroundCharacteristic.getId().toString()))
            .body(result);
    }

    /**
     * GET  /background-characteristics : get all the backgroundCharacteristics.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of backgroundCharacteristics in body
     */
    @GetMapping("/background-characteristics")
    public ResponseEntity<List<BackgroundCharacteristic>> getAllBackgroundCharacteristics(Pageable pageable) {
        log.debug("REST request to get a page of BackgroundCharacteristics");
        Page<BackgroundCharacteristic> page = backgroundCharacteristicRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/background-characteristics");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /background-characteristics/:id : get the "id" backgroundCharacteristic.
     *
     * @param id the id of the backgroundCharacteristic to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the backgroundCharacteristic, or with status 404 (Not Found)
     */
    @GetMapping("/background-characteristics/{id}")
    public ResponseEntity<BackgroundCharacteristic> getBackgroundCharacteristic(@PathVariable Long id) {
        log.debug("REST request to get BackgroundCharacteristic : {}", id);
        Optional<BackgroundCharacteristic> backgroundCharacteristic = backgroundCharacteristicRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(backgroundCharacteristic);
    }

    /**
     * DELETE  /background-characteristics/:id : delete the "id" backgroundCharacteristic.
     *
     * @param id the id of the backgroundCharacteristic to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/background-characteristics/{id}")
    public ResponseEntity<Void> deleteBackgroundCharacteristic(@PathVariable Long id) {
        log.debug("REST request to delete BackgroundCharacteristic : {}", id);
        backgroundCharacteristicRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
