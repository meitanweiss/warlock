package com.warlock.app.web.rest;
import com.warlock.app.domain.Chara;
import com.warlock.app.repository.CharaRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Chara.
 */
@RestController
@RequestMapping("/api")
public class CharaResource {

    private final Logger log = LoggerFactory.getLogger(CharaResource.class);

    private static final String ENTITY_NAME = "chara";

    private final CharaRepository charaRepository;

    public CharaResource(CharaRepository charaRepository) {
        this.charaRepository = charaRepository;
    }

    /**
     * POST  /charas : Create a new chara.
     *
     * @param chara the chara to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chara, or with status 400 (Bad Request) if the chara has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/charas")
    public ResponseEntity<Chara> createChara(@RequestBody Chara chara) throws URISyntaxException {
        log.debug("REST request to save Chara : {}", chara);
        if (chara.getId() != null) {
            throw new BadRequestAlertException("A new chara cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Chara result = charaRepository.save(chara);
        return ResponseEntity.created(new URI("/api/charas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /charas : Updates an existing chara.
     *
     * @param chara the chara to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chara,
     * or with status 400 (Bad Request) if the chara is not valid,
     * or with status 500 (Internal Server Error) if the chara couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/charas")
    public ResponseEntity<Chara> updateChara(@RequestBody Chara chara) throws URISyntaxException {
        log.debug("REST request to update Chara : {}", chara);
        if (chara.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Chara result = charaRepository.save(chara);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chara.getId().toString()))
            .body(result);
    }

    /**
     * GET  /charas : get all the charas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of charas in body
     */
    @GetMapping("/charas")
    public List<Chara> getAllCharas() {
        log.debug("REST request to get all Charas");
        return charaRepository.findAll();
    }

    /**
     * GET  /charas/:id : get the "id" chara.
     *
     * @param id the id of the chara to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chara, or with status 404 (Not Found)
     */
    @GetMapping("/charas/{id}")
    public ResponseEntity<Chara> getChara(@PathVariable Long id) {
        log.debug("REST request to get Chara : {}", id);
        Optional<Chara> chara = charaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chara);
    }

    /**
     * DELETE  /charas/:id : delete the "id" chara.
     *
     * @param id the id of the chara to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/charas/{id}")
    public ResponseEntity<Void> deleteChara(@PathVariable Long id) {
        log.debug("REST request to delete Chara : {}", id);
        charaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
