package com.warlock.app.web.rest;
import com.warlock.app.domain.Trait;
import com.warlock.app.repository.TraitRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Trait.
 */
@RestController
@RequestMapping("/api")
public class TraitResource {

    private final Logger log = LoggerFactory.getLogger(TraitResource.class);

    private static final String ENTITY_NAME = "trait";

    private final TraitRepository traitRepository;

    public TraitResource(TraitRepository traitRepository) {
        this.traitRepository = traitRepository;
    }

    /**
     * POST  /traits : Create a new trait.
     *
     * @param trait the trait to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trait, or with status 400 (Bad Request) if the trait has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/traits")
    public ResponseEntity<Trait> createTrait(@RequestBody Trait trait) throws URISyntaxException {
        log.debug("REST request to save Trait : {}", trait);
        if (trait.getId() != null) {
            throw new BadRequestAlertException("A new trait cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Trait result = traitRepository.save(trait);
        return ResponseEntity.created(new URI("/api/traits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /traits : Updates an existing trait.
     *
     * @param trait the trait to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trait,
     * or with status 400 (Bad Request) if the trait is not valid,
     * or with status 500 (Internal Server Error) if the trait couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/traits")
    public ResponseEntity<Trait> updateTrait(@RequestBody Trait trait) throws URISyntaxException {
        log.debug("REST request to update Trait : {}", trait);
        if (trait.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Trait result = traitRepository.save(trait);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trait.getId().toString()))
            .body(result);
    }

    /**
     * GET  /traits : get all the traits.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of traits in body
     */
    @GetMapping("/traits")
    public List<Trait> getAllTraits() {
        log.debug("REST request to get all Traits");
        return traitRepository.findAll();
    }

    /**
     * GET  /traits/:id : get the "id" trait.
     *
     * @param id the id of the trait to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trait, or with status 404 (Not Found)
     */
    @GetMapping("/traits/{id}")
    public ResponseEntity<Trait> getTrait(@PathVariable Long id) {
        log.debug("REST request to get Trait : {}", id);
        Optional<Trait> trait = traitRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(trait);
    }

    /**
     * DELETE  /traits/:id : delete the "id" trait.
     *
     * @param id the id of the trait to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/traits/{id}")
    public ResponseEntity<Void> deleteTrait(@PathVariable Long id) {
        log.debug("REST request to delete Trait : {}", id);
        traitRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
