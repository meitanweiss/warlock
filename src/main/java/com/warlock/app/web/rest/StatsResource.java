package com.warlock.app.web.rest;
import com.warlock.app.domain.Stats;
import com.warlock.app.repository.StatsRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Stats.
 */
@RestController
@RequestMapping("/api")
public class StatsResource {

    private final Logger log = LoggerFactory.getLogger(StatsResource.class);

    private static final String ENTITY_NAME = "stats";

    private final StatsRepository statsRepository;

    public StatsResource(StatsRepository statsRepository) {
        this.statsRepository = statsRepository;
    }

    /**
     * POST  /stats : Create a new stats.
     *
     * @param stats the stats to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stats, or with status 400 (Bad Request) if the stats has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stats")
    public ResponseEntity<Stats> createStats(@RequestBody Stats stats) throws URISyntaxException {
        log.debug("REST request to save Stats : {}", stats);
        if (stats.getId() != null) {
            throw new BadRequestAlertException("A new stats cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Stats result = statsRepository.save(stats);
        return ResponseEntity.created(new URI("/api/stats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /stats : Updates an existing stats.
     *
     * @param stats the stats to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stats,
     * or with status 400 (Bad Request) if the stats is not valid,
     * or with status 500 (Internal Server Error) if the stats couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stats")
    public ResponseEntity<Stats> updateStats(@RequestBody Stats stats) throws URISyntaxException {
        log.debug("REST request to update Stats : {}", stats);
        if (stats.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Stats result = statsRepository.save(stats);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, stats.getId().toString()))
            .body(result);
    }

    /**
     * GET  /stats : get all the stats.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of stats in body
     */
    @GetMapping("/stats")
    public List<Stats> getAllStats() {
        log.debug("REST request to get all Stats");
        return statsRepository.findAll();
    }

    /**
     * GET  /stats/:id : get the "id" stats.
     *
     * @param id the id of the stats to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stats, or with status 404 (Not Found)
     */
    @GetMapping("/stats/{id}")
    public ResponseEntity<Stats> getStats(@PathVariable Long id) {
        log.debug("REST request to get Stats : {}", id);
        Optional<Stats> stats = statsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(stats);
    }

    /**
     * DELETE  /stats/:id : delete the "id" stats.
     *
     * @param id the id of the stats to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stats/{id}")
    public ResponseEntity<Void> deleteStats(@PathVariable Long id) {
        log.debug("REST request to delete Stats : {}", id);
        statsRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
