package com.warlock.app.web.rest;
import com.warlock.app.domain.Background;
import com.warlock.app.repository.BackgroundRepository;
import com.warlock.app.web.rest.errors.BadRequestAlertException;
import com.warlock.app.web.rest.util.HeaderUtil;
import com.warlock.app.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Background.
 */
@RestController
@RequestMapping("/api")
public class BackgroundResource {

    private final Logger log = LoggerFactory.getLogger(BackgroundResource.class);

    private static final String ENTITY_NAME = "background";

    private final BackgroundRepository backgroundRepository;

    public BackgroundResource(BackgroundRepository backgroundRepository) {
        this.backgroundRepository = backgroundRepository;
    }

    /**
     * POST  /backgrounds : Create a new background.
     *
     * @param background the background to create
     * @return the ResponseEntity with status 201 (Created) and with body the new background, or with status 400 (Bad Request) if the background has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/backgrounds")
    public ResponseEntity<Background> createBackground(@RequestBody Background background) throws URISyntaxException {
        log.debug("REST request to save Background : {}", background);
        if (background.getId() != null) {
            throw new BadRequestAlertException("A new background cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Background result = backgroundRepository.save(background);
        return ResponseEntity.created(new URI("/api/backgrounds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /backgrounds : Updates an existing background.
     *
     * @param background the background to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated background,
     * or with status 400 (Bad Request) if the background is not valid,
     * or with status 500 (Internal Server Error) if the background couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/backgrounds")
    public ResponseEntity<Background> updateBackground(@RequestBody Background background) throws URISyntaxException {
        log.debug("REST request to update Background : {}", background);
        if (background.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Background result = backgroundRepository.save(background);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, background.getId().toString()))
            .body(result);
    }

    /**
     * GET  /backgrounds : get all the backgrounds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of backgrounds in body
     */
    @GetMapping("/backgrounds")
    public ResponseEntity<List<Background>> getAllBackgrounds(Pageable pageable) {
        log.debug("REST request to get a page of Backgrounds");
        Page<Background> page = backgroundRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/backgrounds");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /backgrounds/:id : get the "id" background.
     *
     * @param id the id of the background to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the background, or with status 404 (Not Found)
     */
    @GetMapping("/backgrounds/{id}")
    public ResponseEntity<Background> getBackground(@PathVariable Long id) {
        log.debug("REST request to get Background : {}", id);
        Optional<Background> background = backgroundRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(background);
    }

    /**
     * DELETE  /backgrounds/:id : delete the "id" background.
     *
     * @param id the id of the background to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/backgrounds/{id}")
    public ResponseEntity<Void> deleteBackground(@PathVariable Long id) {
        log.debug("REST request to delete Background : {}", id);
        backgroundRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
