/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { BackgroundUpdateComponent } from 'app/entities/background/background-update.component';
import { BackgroundService } from 'app/entities/background/background.service';
import { Background } from 'app/shared/model/background.model';

describe('Component Tests', () => {
    describe('Background Management Update Component', () => {
        let comp: BackgroundUpdateComponent;
        let fixture: ComponentFixture<BackgroundUpdateComponent>;
        let service: BackgroundService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [BackgroundUpdateComponent]
            })
                .overrideTemplate(BackgroundUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BackgroundUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BackgroundService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Background(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.background = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Background();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.background = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
