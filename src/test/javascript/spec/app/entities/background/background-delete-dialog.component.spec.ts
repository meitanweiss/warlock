/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { BackgroundDeleteDialogComponent } from 'app/entities/background/background-delete-dialog.component';
import { BackgroundService } from 'app/entities/background/background.service';

describe('Component Tests', () => {
    describe('Background Management Delete Component', () => {
        let comp: BackgroundDeleteDialogComponent;
        let fixture: ComponentFixture<BackgroundDeleteDialogComponent>;
        let service: BackgroundService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [BackgroundDeleteDialogComponent]
            })
                .overrideTemplate(BackgroundDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BackgroundDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BackgroundService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
