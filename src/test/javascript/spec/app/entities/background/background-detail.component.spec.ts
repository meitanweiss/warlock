/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { BackgroundDetailComponent } from 'app/entities/background/background-detail.component';
import { Background } from 'app/shared/model/background.model';

describe('Component Tests', () => {
    describe('Background Management Detail Component', () => {
        let comp: BackgroundDetailComponent;
        let fixture: ComponentFixture<BackgroundDetailComponent>;
        const route = ({ data: of({ background: new Background(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [BackgroundDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(BackgroundDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BackgroundDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.background).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
