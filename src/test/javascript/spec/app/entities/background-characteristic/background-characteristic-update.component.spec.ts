/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { BackgroundCharacteristicUpdateComponent } from 'app/entities/background-characteristic/background-characteristic-update.component';
import { BackgroundCharacteristicService } from 'app/entities/background-characteristic/background-characteristic.service';
import { BackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';

describe('Component Tests', () => {
    describe('BackgroundCharacteristic Management Update Component', () => {
        let comp: BackgroundCharacteristicUpdateComponent;
        let fixture: ComponentFixture<BackgroundCharacteristicUpdateComponent>;
        let service: BackgroundCharacteristicService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [BackgroundCharacteristicUpdateComponent]
            })
                .overrideTemplate(BackgroundCharacteristicUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BackgroundCharacteristicUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BackgroundCharacteristicService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new BackgroundCharacteristic(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.backgroundCharacteristic = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new BackgroundCharacteristic();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.backgroundCharacteristic = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
