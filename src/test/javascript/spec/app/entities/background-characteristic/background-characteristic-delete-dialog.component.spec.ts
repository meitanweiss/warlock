/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { BackgroundCharacteristicDeleteDialogComponent } from 'app/entities/background-characteristic/background-characteristic-delete-dialog.component';
import { BackgroundCharacteristicService } from 'app/entities/background-characteristic/background-characteristic.service';

describe('Component Tests', () => {
    describe('BackgroundCharacteristic Management Delete Component', () => {
        let comp: BackgroundCharacteristicDeleteDialogComponent;
        let fixture: ComponentFixture<BackgroundCharacteristicDeleteDialogComponent>;
        let service: BackgroundCharacteristicService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [BackgroundCharacteristicDeleteDialogComponent]
            })
                .overrideTemplate(BackgroundCharacteristicDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BackgroundCharacteristicDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BackgroundCharacteristicService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
