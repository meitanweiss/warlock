/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { BackgroundCharacteristicDetailComponent } from 'app/entities/background-characteristic/background-characteristic-detail.component';
import { BackgroundCharacteristic } from 'app/shared/model/background-characteristic.model';

describe('Component Tests', () => {
    describe('BackgroundCharacteristic Management Detail Component', () => {
        let comp: BackgroundCharacteristicDetailComponent;
        let fixture: ComponentFixture<BackgroundCharacteristicDetailComponent>;
        const route = ({ data: of({ backgroundCharacteristic: new BackgroundCharacteristic(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [BackgroundCharacteristicDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(BackgroundCharacteristicDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BackgroundCharacteristicDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.backgroundCharacteristic).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
