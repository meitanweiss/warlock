/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { CharaDetailComponent } from 'app/entities/chara/chara-detail.component';
import { Chara } from 'app/shared/model/chara.model';

describe('Component Tests', () => {
    describe('Chara Management Detail Component', () => {
        let comp: CharaDetailComponent;
        let fixture: ComponentFixture<CharaDetailComponent>;
        const route = ({ data: of({ chara: new Chara(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [CharaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CharaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CharaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.chara).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
