/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarlockTestModule } from '../../../test.module';
import { CharaComponent } from 'app/entities/chara/chara.component';
import { CharaService } from 'app/entities/chara/chara.service';
import { Chara } from 'app/shared/model/chara.model';

describe('Component Tests', () => {
    describe('Chara Management Component', () => {
        let comp: CharaComponent;
        let fixture: ComponentFixture<CharaComponent>;
        let service: CharaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [CharaComponent],
                providers: []
            })
                .overrideTemplate(CharaComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CharaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharaService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Chara(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.charas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
