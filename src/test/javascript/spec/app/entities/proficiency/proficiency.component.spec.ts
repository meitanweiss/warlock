/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarlockTestModule } from '../../../test.module';
import { ProficiencyComponent } from 'app/entities/proficiency/proficiency.component';
import { ProficiencyService } from 'app/entities/proficiency/proficiency.service';
import { Proficiency } from 'app/shared/model/proficiency.model';

describe('Component Tests', () => {
    describe('Proficiency Management Component', () => {
        let comp: ProficiencyComponent;
        let fixture: ComponentFixture<ProficiencyComponent>;
        let service: ProficiencyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [ProficiencyComponent],
                providers: []
            })
                .overrideTemplate(ProficiencyComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProficiencyComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProficiencyService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Proficiency(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.proficiencies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
