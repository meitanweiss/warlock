/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { ProficiencyDeleteDialogComponent } from 'app/entities/proficiency/proficiency-delete-dialog.component';
import { ProficiencyService } from 'app/entities/proficiency/proficiency.service';

describe('Component Tests', () => {
    describe('Proficiency Management Delete Component', () => {
        let comp: ProficiencyDeleteDialogComponent;
        let fixture: ComponentFixture<ProficiencyDeleteDialogComponent>;
        let service: ProficiencyService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [ProficiencyDeleteDialogComponent]
            })
                .overrideTemplate(ProficiencyDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProficiencyDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProficiencyService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
