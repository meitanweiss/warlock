/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { ProficiencyDetailComponent } from 'app/entities/proficiency/proficiency-detail.component';
import { Proficiency } from 'app/shared/model/proficiency.model';

describe('Component Tests', () => {
    describe('Proficiency Management Detail Component', () => {
        let comp: ProficiencyDetailComponent;
        let fixture: ComponentFixture<ProficiencyDetailComponent>;
        const route = ({ data: of({ proficiency: new Proficiency(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [ProficiencyDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ProficiencyDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProficiencyDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.proficiency).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
