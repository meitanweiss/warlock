/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { ProficiencyUpdateComponent } from 'app/entities/proficiency/proficiency-update.component';
import { ProficiencyService } from 'app/entities/proficiency/proficiency.service';
import { Proficiency } from 'app/shared/model/proficiency.model';

describe('Component Tests', () => {
    describe('Proficiency Management Update Component', () => {
        let comp: ProficiencyUpdateComponent;
        let fixture: ComponentFixture<ProficiencyUpdateComponent>;
        let service: ProficiencyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [ProficiencyUpdateComponent]
            })
                .overrideTemplate(ProficiencyUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProficiencyUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProficiencyService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Proficiency(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.proficiency = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Proficiency();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.proficiency = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
