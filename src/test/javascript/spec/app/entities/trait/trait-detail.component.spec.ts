/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { TraitDetailComponent } from 'app/entities/trait/trait-detail.component';
import { Trait } from 'app/shared/model/trait.model';

describe('Component Tests', () => {
    describe('Trait Management Detail Component', () => {
        let comp: TraitDetailComponent;
        let fixture: ComponentFixture<TraitDetailComponent>;
        const route = ({ data: of({ trait: new Trait(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [TraitDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(TraitDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TraitDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.trait).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
