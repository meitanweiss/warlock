/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarlockTestModule } from '../../../test.module';
import { TraitComponent } from 'app/entities/trait/trait.component';
import { TraitService } from 'app/entities/trait/trait.service';
import { Trait } from 'app/shared/model/trait.model';

describe('Component Tests', () => {
    describe('Trait Management Component', () => {
        let comp: TraitComponent;
        let fixture: ComponentFixture<TraitComponent>;
        let service: TraitService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [TraitComponent],
                providers: []
            })
                .overrideTemplate(TraitComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(TraitComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraitService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Trait(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.traits[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
