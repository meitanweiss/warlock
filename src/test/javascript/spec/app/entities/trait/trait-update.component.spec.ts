/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { TraitUpdateComponent } from 'app/entities/trait/trait-update.component';
import { TraitService } from 'app/entities/trait/trait.service';
import { Trait } from 'app/shared/model/trait.model';

describe('Component Tests', () => {
    describe('Trait Management Update Component', () => {
        let comp: TraitUpdateComponent;
        let fixture: ComponentFixture<TraitUpdateComponent>;
        let service: TraitService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [TraitUpdateComponent]
            })
                .overrideTemplate(TraitUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(TraitUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraitService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Trait(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.trait = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Trait();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.trait = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
