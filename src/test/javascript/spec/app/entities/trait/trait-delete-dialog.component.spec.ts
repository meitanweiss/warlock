/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { TraitDeleteDialogComponent } from 'app/entities/trait/trait-delete-dialog.component';
import { TraitService } from 'app/entities/trait/trait.service';

describe('Component Tests', () => {
    describe('Trait Management Delete Component', () => {
        let comp: TraitDeleteDialogComponent;
        let fixture: ComponentFixture<TraitDeleteDialogComponent>;
        let service: TraitService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [TraitDeleteDialogComponent]
            })
                .overrideTemplate(TraitDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TraitDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraitService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
