/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { CharacterClassDetailComponent } from 'app/entities/character-class/character-class-detail.component';
import { CharacterClass } from 'app/shared/model/character-class.model';

describe('Component Tests', () => {
    describe('CharacterClass Management Detail Component', () => {
        let comp: CharacterClassDetailComponent;
        let fixture: ComponentFixture<CharacterClassDetailComponent>;
        const route = ({ data: of({ characterClass: new CharacterClass(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [CharacterClassDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CharacterClassDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CharacterClassDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.characterClass).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
