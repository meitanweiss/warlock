/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { CharacterClassUpdateComponent } from 'app/entities/character-class/character-class-update.component';
import { CharacterClassService } from 'app/entities/character-class/character-class.service';
import { CharacterClass } from 'app/shared/model/character-class.model';

describe('Component Tests', () => {
    describe('CharacterClass Management Update Component', () => {
        let comp: CharacterClassUpdateComponent;
        let fixture: ComponentFixture<CharacterClassUpdateComponent>;
        let service: CharacterClassService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [CharacterClassUpdateComponent]
            })
                .overrideTemplate(CharacterClassUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CharacterClassUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharacterClassService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CharacterClass(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.characterClass = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CharacterClass();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.characterClass = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
