/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { CharacterClassDeleteDialogComponent } from 'app/entities/character-class/character-class-delete-dialog.component';
import { CharacterClassService } from 'app/entities/character-class/character-class.service';

describe('Component Tests', () => {
    describe('CharacterClass Management Delete Component', () => {
        let comp: CharacterClassDeleteDialogComponent;
        let fixture: ComponentFixture<CharacterClassDeleteDialogComponent>;
        let service: CharacterClassService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [CharacterClassDeleteDialogComponent]
            })
                .overrideTemplate(CharacterClassDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CharacterClassDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharacterClassService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
