/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { RacialTraitsUpdateComponent } from 'app/entities/racial-traits/racial-traits-update.component';
import { RacialTraitsService } from 'app/entities/racial-traits/racial-traits.service';
import { RacialTraits } from 'app/shared/model/racial-traits.model';

describe('Component Tests', () => {
    describe('RacialTraits Management Update Component', () => {
        let comp: RacialTraitsUpdateComponent;
        let fixture: ComponentFixture<RacialTraitsUpdateComponent>;
        let service: RacialTraitsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [RacialTraitsUpdateComponent]
            })
                .overrideTemplate(RacialTraitsUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RacialTraitsUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RacialTraitsService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RacialTraits(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.racialTraits = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RacialTraits();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.racialTraits = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
