/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { RacialTraitsDeleteDialogComponent } from 'app/entities/racial-traits/racial-traits-delete-dialog.component';
import { RacialTraitsService } from 'app/entities/racial-traits/racial-traits.service';

describe('Component Tests', () => {
    describe('RacialTraits Management Delete Component', () => {
        let comp: RacialTraitsDeleteDialogComponent;
        let fixture: ComponentFixture<RacialTraitsDeleteDialogComponent>;
        let service: RacialTraitsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [RacialTraitsDeleteDialogComponent]
            })
                .overrideTemplate(RacialTraitsDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RacialTraitsDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RacialTraitsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
