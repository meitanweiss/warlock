/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { RacialTraitsDetailComponent } from 'app/entities/racial-traits/racial-traits-detail.component';
import { RacialTraits } from 'app/shared/model/racial-traits.model';

describe('Component Tests', () => {
    describe('RacialTraits Management Detail Component', () => {
        let comp: RacialTraitsDetailComponent;
        let fixture: ComponentFixture<RacialTraitsDetailComponent>;
        const route = ({ data: of({ racialTraits: new RacialTraits(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [RacialTraitsDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RacialTraitsDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RacialTraitsDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.racialTraits).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
