/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { RaceDeleteDialogComponent } from 'app/entities/race/race-delete-dialog.component';
import { RaceService } from 'app/entities/race/race.service';

describe('Component Tests', () => {
    describe('Race Management Delete Component', () => {
        let comp: RaceDeleteDialogComponent;
        let fixture: ComponentFixture<RaceDeleteDialogComponent>;
        let service: RaceService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [RaceDeleteDialogComponent]
            })
                .overrideTemplate(RaceDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
