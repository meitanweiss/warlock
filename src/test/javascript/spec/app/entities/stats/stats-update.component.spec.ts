/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { WarlockTestModule } from '../../../test.module';
import { StatsUpdateComponent } from 'app/entities/stats/stats-update.component';
import { StatsService } from 'app/entities/stats/stats.service';
import { Stats } from 'app/shared/model/stats.model';

describe('Component Tests', () => {
    describe('Stats Management Update Component', () => {
        let comp: StatsUpdateComponent;
        let fixture: ComponentFixture<StatsUpdateComponent>;
        let service: StatsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [StatsUpdateComponent]
            })
                .overrideTemplate(StatsUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(StatsUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StatsService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Stats(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.stats = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Stats();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.stats = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
