/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarlockTestModule } from '../../../test.module';
import { AbilityDeleteDialogComponent } from 'app/entities/ability/ability-delete-dialog.component';
import { AbilityService } from 'app/entities/ability/ability.service';

describe('Component Tests', () => {
    describe('Ability Management Delete Component', () => {
        let comp: AbilityDeleteDialogComponent;
        let fixture: ComponentFixture<AbilityDeleteDialogComponent>;
        let service: AbilityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [WarlockTestModule],
                declarations: [AbilityDeleteDialogComponent]
            })
                .overrideTemplate(AbilityDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AbilityDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AbilityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
