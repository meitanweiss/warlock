package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.BackgroundCharacteristic;
import com.warlock.app.repository.BackgroundCharacteristicRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BackgroundCharacteristicResource REST controller.
 *
 * @see BackgroundCharacteristicResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class BackgroundCharacteristicResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private BackgroundCharacteristicRepository backgroundCharacteristicRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBackgroundCharacteristicMockMvc;

    private BackgroundCharacteristic backgroundCharacteristic;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BackgroundCharacteristicResource backgroundCharacteristicResource = new BackgroundCharacteristicResource(backgroundCharacteristicRepository);
        this.restBackgroundCharacteristicMockMvc = MockMvcBuilders.standaloneSetup(backgroundCharacteristicResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BackgroundCharacteristic createEntity(EntityManager em) {
        BackgroundCharacteristic backgroundCharacteristic = new BackgroundCharacteristic()
            .description(DEFAULT_DESCRIPTION);
        return backgroundCharacteristic;
    }

    @Before
    public void initTest() {
        backgroundCharacteristic = createEntity(em);
    }

    @Test
    @Transactional
    public void createBackgroundCharacteristic() throws Exception {
        int databaseSizeBeforeCreate = backgroundCharacteristicRepository.findAll().size();

        // Create the BackgroundCharacteristic
        restBackgroundCharacteristicMockMvc.perform(post("/api/background-characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(backgroundCharacteristic)))
            .andExpect(status().isCreated());

        // Validate the BackgroundCharacteristic in the database
        List<BackgroundCharacteristic> backgroundCharacteristicList = backgroundCharacteristicRepository.findAll();
        assertThat(backgroundCharacteristicList).hasSize(databaseSizeBeforeCreate + 1);
        BackgroundCharacteristic testBackgroundCharacteristic = backgroundCharacteristicList.get(backgroundCharacteristicList.size() - 1);
        assertThat(testBackgroundCharacteristic.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createBackgroundCharacteristicWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = backgroundCharacteristicRepository.findAll().size();

        // Create the BackgroundCharacteristic with an existing ID
        backgroundCharacteristic.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBackgroundCharacteristicMockMvc.perform(post("/api/background-characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(backgroundCharacteristic)))
            .andExpect(status().isBadRequest());

        // Validate the BackgroundCharacteristic in the database
        List<BackgroundCharacteristic> backgroundCharacteristicList = backgroundCharacteristicRepository.findAll();
        assertThat(backgroundCharacteristicList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBackgroundCharacteristics() throws Exception {
        // Initialize the database
        backgroundCharacteristicRepository.saveAndFlush(backgroundCharacteristic);

        // Get all the backgroundCharacteristicList
        restBackgroundCharacteristicMockMvc.perform(get("/api/background-characteristics?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(backgroundCharacteristic.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getBackgroundCharacteristic() throws Exception {
        // Initialize the database
        backgroundCharacteristicRepository.saveAndFlush(backgroundCharacteristic);

        // Get the backgroundCharacteristic
        restBackgroundCharacteristicMockMvc.perform(get("/api/background-characteristics/{id}", backgroundCharacteristic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(backgroundCharacteristic.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBackgroundCharacteristic() throws Exception {
        // Get the backgroundCharacteristic
        restBackgroundCharacteristicMockMvc.perform(get("/api/background-characteristics/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBackgroundCharacteristic() throws Exception {
        // Initialize the database
        backgroundCharacteristicRepository.saveAndFlush(backgroundCharacteristic);

        int databaseSizeBeforeUpdate = backgroundCharacteristicRepository.findAll().size();

        // Update the backgroundCharacteristic
        BackgroundCharacteristic updatedBackgroundCharacteristic = backgroundCharacteristicRepository.findById(backgroundCharacteristic.getId()).get();
        // Disconnect from session so that the updates on updatedBackgroundCharacteristic are not directly saved in db
        em.detach(updatedBackgroundCharacteristic);
        updatedBackgroundCharacteristic
            .description(UPDATED_DESCRIPTION);

        restBackgroundCharacteristicMockMvc.perform(put("/api/background-characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBackgroundCharacteristic)))
            .andExpect(status().isOk());

        // Validate the BackgroundCharacteristic in the database
        List<BackgroundCharacteristic> backgroundCharacteristicList = backgroundCharacteristicRepository.findAll();
        assertThat(backgroundCharacteristicList).hasSize(databaseSizeBeforeUpdate);
        BackgroundCharacteristic testBackgroundCharacteristic = backgroundCharacteristicList.get(backgroundCharacteristicList.size() - 1);
        assertThat(testBackgroundCharacteristic.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingBackgroundCharacteristic() throws Exception {
        int databaseSizeBeforeUpdate = backgroundCharacteristicRepository.findAll().size();

        // Create the BackgroundCharacteristic

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBackgroundCharacteristicMockMvc.perform(put("/api/background-characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(backgroundCharacteristic)))
            .andExpect(status().isBadRequest());

        // Validate the BackgroundCharacteristic in the database
        List<BackgroundCharacteristic> backgroundCharacteristicList = backgroundCharacteristicRepository.findAll();
        assertThat(backgroundCharacteristicList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBackgroundCharacteristic() throws Exception {
        // Initialize the database
        backgroundCharacteristicRepository.saveAndFlush(backgroundCharacteristic);

        int databaseSizeBeforeDelete = backgroundCharacteristicRepository.findAll().size();

        // Delete the backgroundCharacteristic
        restBackgroundCharacteristicMockMvc.perform(delete("/api/background-characteristics/{id}", backgroundCharacteristic.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BackgroundCharacteristic> backgroundCharacteristicList = backgroundCharacteristicRepository.findAll();
        assertThat(backgroundCharacteristicList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BackgroundCharacteristic.class);
        BackgroundCharacteristic backgroundCharacteristic1 = new BackgroundCharacteristic();
        backgroundCharacteristic1.setId(1L);
        BackgroundCharacteristic backgroundCharacteristic2 = new BackgroundCharacteristic();
        backgroundCharacteristic2.setId(backgroundCharacteristic1.getId());
        assertThat(backgroundCharacteristic1).isEqualTo(backgroundCharacteristic2);
        backgroundCharacteristic2.setId(2L);
        assertThat(backgroundCharacteristic1).isNotEqualTo(backgroundCharacteristic2);
        backgroundCharacteristic1.setId(null);
        assertThat(backgroundCharacteristic1).isNotEqualTo(backgroundCharacteristic2);
    }
}
