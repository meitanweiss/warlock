package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.Background;
import com.warlock.app.repository.BackgroundRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BackgroundResource REST controller.
 *
 * @see BackgroundResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class BackgroundResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private BackgroundRepository backgroundRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBackgroundMockMvc;

    private Background background;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BackgroundResource backgroundResource = new BackgroundResource(backgroundRepository);
        this.restBackgroundMockMvc = MockMvcBuilders.standaloneSetup(backgroundResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Background createEntity(EntityManager em) {
        Background background = new Background()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return background;
    }

    @Before
    public void initTest() {
        background = createEntity(em);
    }

    @Test
    @Transactional
    public void createBackground() throws Exception {
        int databaseSizeBeforeCreate = backgroundRepository.findAll().size();

        // Create the Background
        restBackgroundMockMvc.perform(post("/api/backgrounds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(background)))
            .andExpect(status().isCreated());

        // Validate the Background in the database
        List<Background> backgroundList = backgroundRepository.findAll();
        assertThat(backgroundList).hasSize(databaseSizeBeforeCreate + 1);
        Background testBackground = backgroundList.get(backgroundList.size() - 1);
        assertThat(testBackground.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBackground.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createBackgroundWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = backgroundRepository.findAll().size();

        // Create the Background with an existing ID
        background.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBackgroundMockMvc.perform(post("/api/backgrounds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(background)))
            .andExpect(status().isBadRequest());

        // Validate the Background in the database
        List<Background> backgroundList = backgroundRepository.findAll();
        assertThat(backgroundList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBackgrounds() throws Exception {
        // Initialize the database
        backgroundRepository.saveAndFlush(background);

        // Get all the backgroundList
        restBackgroundMockMvc.perform(get("/api/backgrounds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(background.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getBackground() throws Exception {
        // Initialize the database
        backgroundRepository.saveAndFlush(background);

        // Get the background
        restBackgroundMockMvc.perform(get("/api/backgrounds/{id}", background.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(background.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBackground() throws Exception {
        // Get the background
        restBackgroundMockMvc.perform(get("/api/backgrounds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBackground() throws Exception {
        // Initialize the database
        backgroundRepository.saveAndFlush(background);

        int databaseSizeBeforeUpdate = backgroundRepository.findAll().size();

        // Update the background
        Background updatedBackground = backgroundRepository.findById(background.getId()).get();
        // Disconnect from session so that the updates on updatedBackground are not directly saved in db
        em.detach(updatedBackground);
        updatedBackground
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restBackgroundMockMvc.perform(put("/api/backgrounds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBackground)))
            .andExpect(status().isOk());

        // Validate the Background in the database
        List<Background> backgroundList = backgroundRepository.findAll();
        assertThat(backgroundList).hasSize(databaseSizeBeforeUpdate);
        Background testBackground = backgroundList.get(backgroundList.size() - 1);
        assertThat(testBackground.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBackground.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingBackground() throws Exception {
        int databaseSizeBeforeUpdate = backgroundRepository.findAll().size();

        // Create the Background

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBackgroundMockMvc.perform(put("/api/backgrounds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(background)))
            .andExpect(status().isBadRequest());

        // Validate the Background in the database
        List<Background> backgroundList = backgroundRepository.findAll();
        assertThat(backgroundList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBackground() throws Exception {
        // Initialize the database
        backgroundRepository.saveAndFlush(background);

        int databaseSizeBeforeDelete = backgroundRepository.findAll().size();

        // Delete the background
        restBackgroundMockMvc.perform(delete("/api/backgrounds/{id}", background.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Background> backgroundList = backgroundRepository.findAll();
        assertThat(backgroundList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Background.class);
        Background background1 = new Background();
        background1.setId(1L);
        Background background2 = new Background();
        background2.setId(background1.getId());
        assertThat(background1).isEqualTo(background2);
        background2.setId(2L);
        assertThat(background1).isNotEqualTo(background2);
        background1.setId(null);
        assertThat(background1).isNotEqualTo(background2);
    }
}
