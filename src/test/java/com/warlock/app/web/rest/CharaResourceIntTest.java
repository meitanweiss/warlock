package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.Chara;
import com.warlock.app.repository.CharaRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CharaResource REST controller.
 *
 * @see CharaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class CharaResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ALINGMENT = "AAAAAAAAAA";
    private static final String UPDATED_ALINGMENT = "BBBBBBBBBB";

    private static final String DEFAULT_FAITH = "AAAAAAAAAA";
    private static final String UPDATED_FAITH = "BBBBBBBBBB";

    private static final String DEFAULT_LIFESTYLE = "AAAAAAAAAA";
    private static final String UPDATED_LIFESTYLE = "BBBBBBBBBB";

    private static final String DEFAULT_HAIR = "AAAAAAAAAA";
    private static final String UPDATED_HAIR = "BBBBBBBBBB";

    private static final String DEFAULT_SKIN = "AAAAAAAAAA";
    private static final String UPDATED_SKIN = "BBBBBBBBBB";

    private static final String DEFAULT_EYES = "AAAAAAAAAA";
    private static final String UPDATED_EYES = "BBBBBBBBBB";

    private static final Integer DEFAULT_HEIGHT = 1;
    private static final Integer UPDATED_HEIGHT = 2;

    private static final Integer DEFAULT_WEIGHT = 1;
    private static final Integer UPDATED_WEIGHT = 2;

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;

    private static final String DEFAULT_FLAWS = "AAAAAAAAAA";
    private static final String UPDATED_FLAWS = "BBBBBBBBBB";

    private static final String DEFAULT_SPECIAL = "AAAAAAAAAA";
    private static final String UPDATED_SPECIAL = "BBBBBBBBBB";

    private static final String DEFAULT_GENDER = "AAAAAAAAAA";
    private static final String UPDATED_GENDER = "BBBBBBBBBB";

    private static final String DEFAULT_PERONALITY = "AAAAAAAAAA";
    private static final String UPDATED_PERONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_IDEALS = "AAAAAAAAAA";
    private static final String UPDATED_IDEALS = "BBBBBBBBBB";

    private static final String DEFAULT_BONDS = "AAAAAAAAAA";
    private static final String UPDATED_BONDS = "BBBBBBBBBB";

    @Autowired
    private CharaRepository charaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCharaMockMvc;

    private Chara chara;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CharaResource charaResource = new CharaResource(charaRepository);
        this.restCharaMockMvc = MockMvcBuilders.standaloneSetup(charaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Chara createEntity(EntityManager em) {
        Chara chara = new Chara()
            .name(DEFAULT_NAME)
            .alingment(DEFAULT_ALINGMENT)
            .faith(DEFAULT_FAITH)
            .lifestyle(DEFAULT_LIFESTYLE)
            .hair(DEFAULT_HAIR)
            .skin(DEFAULT_SKIN)
            .eyes(DEFAULT_EYES)
            .height(DEFAULT_HEIGHT)
            .weight(DEFAULT_WEIGHT)
            .age(DEFAULT_AGE)
            .flaws(DEFAULT_FLAWS)
            .special(DEFAULT_SPECIAL)
            .gender(DEFAULT_GENDER)
            .peronality(DEFAULT_PERONALITY)
            .ideals(DEFAULT_IDEALS)
            .bonds(DEFAULT_BONDS);
        return chara;
    }

    @Before
    public void initTest() {
        chara = createEntity(em);
    }

    @Test
    @Transactional
    public void createChara() throws Exception {
        int databaseSizeBeforeCreate = charaRepository.findAll().size();

        // Create the Chara
        restCharaMockMvc.perform(post("/api/charas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chara)))
            .andExpect(status().isCreated());

        // Validate the Chara in the database
        List<Chara> charaList = charaRepository.findAll();
        assertThat(charaList).hasSize(databaseSizeBeforeCreate + 1);
        Chara testChara = charaList.get(charaList.size() - 1);
        assertThat(testChara.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testChara.getAlingment()).isEqualTo(DEFAULT_ALINGMENT);
        assertThat(testChara.getFaith()).isEqualTo(DEFAULT_FAITH);
        assertThat(testChara.getLifestyle()).isEqualTo(DEFAULT_LIFESTYLE);
        assertThat(testChara.getHair()).isEqualTo(DEFAULT_HAIR);
        assertThat(testChara.getSkin()).isEqualTo(DEFAULT_SKIN);
        assertThat(testChara.getEyes()).isEqualTo(DEFAULT_EYES);
        assertThat(testChara.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testChara.getWeight()).isEqualTo(DEFAULT_WEIGHT);
        assertThat(testChara.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testChara.getFlaws()).isEqualTo(DEFAULT_FLAWS);
        assertThat(testChara.getSpecial()).isEqualTo(DEFAULT_SPECIAL);
        assertThat(testChara.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testChara.getPeronality()).isEqualTo(DEFAULT_PERONALITY);
        assertThat(testChara.getIdeals()).isEqualTo(DEFAULT_IDEALS);
        assertThat(testChara.getBonds()).isEqualTo(DEFAULT_BONDS);
    }

    @Test
    @Transactional
    public void createCharaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = charaRepository.findAll().size();

        // Create the Chara with an existing ID
        chara.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCharaMockMvc.perform(post("/api/charas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chara)))
            .andExpect(status().isBadRequest());

        // Validate the Chara in the database
        List<Chara> charaList = charaRepository.findAll();
        assertThat(charaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCharas() throws Exception {
        // Initialize the database
        charaRepository.saveAndFlush(chara);

        // Get all the charaList
        restCharaMockMvc.perform(get("/api/charas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chara.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].alingment").value(hasItem(DEFAULT_ALINGMENT.toString())))
            .andExpect(jsonPath("$.[*].faith").value(hasItem(DEFAULT_FAITH.toString())))
            .andExpect(jsonPath("$.[*].lifestyle").value(hasItem(DEFAULT_LIFESTYLE.toString())))
            .andExpect(jsonPath("$.[*].hair").value(hasItem(DEFAULT_HAIR.toString())))
            .andExpect(jsonPath("$.[*].skin").value(hasItem(DEFAULT_SKIN.toString())))
            .andExpect(jsonPath("$.[*].eyes").value(hasItem(DEFAULT_EYES.toString())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT)))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].flaws").value(hasItem(DEFAULT_FLAWS.toString())))
            .andExpect(jsonPath("$.[*].special").value(hasItem(DEFAULT_SPECIAL.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].peronality").value(hasItem(DEFAULT_PERONALITY.toString())))
            .andExpect(jsonPath("$.[*].ideals").value(hasItem(DEFAULT_IDEALS.toString())))
            .andExpect(jsonPath("$.[*].bonds").value(hasItem(DEFAULT_BONDS.toString())));
    }
    
    @Test
    @Transactional
    public void getChara() throws Exception {
        // Initialize the database
        charaRepository.saveAndFlush(chara);

        // Get the chara
        restCharaMockMvc.perform(get("/api/charas/{id}", chara.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chara.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.alingment").value(DEFAULT_ALINGMENT.toString()))
            .andExpect(jsonPath("$.faith").value(DEFAULT_FAITH.toString()))
            .andExpect(jsonPath("$.lifestyle").value(DEFAULT_LIFESTYLE.toString()))
            .andExpect(jsonPath("$.hair").value(DEFAULT_HAIR.toString()))
            .andExpect(jsonPath("$.skin").value(DEFAULT_SKIN.toString()))
            .andExpect(jsonPath("$.eyes").value(DEFAULT_EYES.toString()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.flaws").value(DEFAULT_FLAWS.toString()))
            .andExpect(jsonPath("$.special").value(DEFAULT_SPECIAL.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.peronality").value(DEFAULT_PERONALITY.toString()))
            .andExpect(jsonPath("$.ideals").value(DEFAULT_IDEALS.toString()))
            .andExpect(jsonPath("$.bonds").value(DEFAULT_BONDS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingChara() throws Exception {
        // Get the chara
        restCharaMockMvc.perform(get("/api/charas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChara() throws Exception {
        // Initialize the database
        charaRepository.saveAndFlush(chara);

        int databaseSizeBeforeUpdate = charaRepository.findAll().size();

        // Update the chara
        Chara updatedChara = charaRepository.findById(chara.getId()).get();
        // Disconnect from session so that the updates on updatedChara are not directly saved in db
        em.detach(updatedChara);
        updatedChara
            .name(UPDATED_NAME)
            .alingment(UPDATED_ALINGMENT)
            .faith(UPDATED_FAITH)
            .lifestyle(UPDATED_LIFESTYLE)
            .hair(UPDATED_HAIR)
            .skin(UPDATED_SKIN)
            .eyes(UPDATED_EYES)
            .height(UPDATED_HEIGHT)
            .weight(UPDATED_WEIGHT)
            .age(UPDATED_AGE)
            .flaws(UPDATED_FLAWS)
            .special(UPDATED_SPECIAL)
            .gender(UPDATED_GENDER)
            .peronality(UPDATED_PERONALITY)
            .ideals(UPDATED_IDEALS)
            .bonds(UPDATED_BONDS);

        restCharaMockMvc.perform(put("/api/charas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChara)))
            .andExpect(status().isOk());

        // Validate the Chara in the database
        List<Chara> charaList = charaRepository.findAll();
        assertThat(charaList).hasSize(databaseSizeBeforeUpdate);
        Chara testChara = charaList.get(charaList.size() - 1);
        assertThat(testChara.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChara.getAlingment()).isEqualTo(UPDATED_ALINGMENT);
        assertThat(testChara.getFaith()).isEqualTo(UPDATED_FAITH);
        assertThat(testChara.getLifestyle()).isEqualTo(UPDATED_LIFESTYLE);
        assertThat(testChara.getHair()).isEqualTo(UPDATED_HAIR);
        assertThat(testChara.getSkin()).isEqualTo(UPDATED_SKIN);
        assertThat(testChara.getEyes()).isEqualTo(UPDATED_EYES);
        assertThat(testChara.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testChara.getWeight()).isEqualTo(UPDATED_WEIGHT);
        assertThat(testChara.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testChara.getFlaws()).isEqualTo(UPDATED_FLAWS);
        assertThat(testChara.getSpecial()).isEqualTo(UPDATED_SPECIAL);
        assertThat(testChara.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testChara.getPeronality()).isEqualTo(UPDATED_PERONALITY);
        assertThat(testChara.getIdeals()).isEqualTo(UPDATED_IDEALS);
        assertThat(testChara.getBonds()).isEqualTo(UPDATED_BONDS);
    }

    @Test
    @Transactional
    public void updateNonExistingChara() throws Exception {
        int databaseSizeBeforeUpdate = charaRepository.findAll().size();

        // Create the Chara

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCharaMockMvc.perform(put("/api/charas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chara)))
            .andExpect(status().isBadRequest());

        // Validate the Chara in the database
        List<Chara> charaList = charaRepository.findAll();
        assertThat(charaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChara() throws Exception {
        // Initialize the database
        charaRepository.saveAndFlush(chara);

        int databaseSizeBeforeDelete = charaRepository.findAll().size();

        // Delete the chara
        restCharaMockMvc.perform(delete("/api/charas/{id}", chara.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Chara> charaList = charaRepository.findAll();
        assertThat(charaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Chara.class);
        Chara chara1 = new Chara();
        chara1.setId(1L);
        Chara chara2 = new Chara();
        chara2.setId(chara1.getId());
        assertThat(chara1).isEqualTo(chara2);
        chara2.setId(2L);
        assertThat(chara1).isNotEqualTo(chara2);
        chara1.setId(null);
        assertThat(chara1).isNotEqualTo(chara2);
    }
}
