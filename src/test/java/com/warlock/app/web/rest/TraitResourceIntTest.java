package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.Trait;
import com.warlock.app.repository.TraitRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TraitResource REST controller.
 *
 * @see TraitResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class TraitResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private TraitRepository traitRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTraitMockMvc;

    private Trait trait;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TraitResource traitResource = new TraitResource(traitRepository);
        this.restTraitMockMvc = MockMvcBuilders.standaloneSetup(traitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trait createEntity(EntityManager em) {
        Trait trait = new Trait()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return trait;
    }

    @Before
    public void initTest() {
        trait = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrait() throws Exception {
        int databaseSizeBeforeCreate = traitRepository.findAll().size();

        // Create the Trait
        restTraitMockMvc.perform(post("/api/traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trait)))
            .andExpect(status().isCreated());

        // Validate the Trait in the database
        List<Trait> traitList = traitRepository.findAll();
        assertThat(traitList).hasSize(databaseSizeBeforeCreate + 1);
        Trait testTrait = traitList.get(traitList.size() - 1);
        assertThat(testTrait.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTrait.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createTraitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = traitRepository.findAll().size();

        // Create the Trait with an existing ID
        trait.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTraitMockMvc.perform(post("/api/traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trait)))
            .andExpect(status().isBadRequest());

        // Validate the Trait in the database
        List<Trait> traitList = traitRepository.findAll();
        assertThat(traitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTraits() throws Exception {
        // Initialize the database
        traitRepository.saveAndFlush(trait);

        // Get all the traitList
        restTraitMockMvc.perform(get("/api/traits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trait.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getTrait() throws Exception {
        // Initialize the database
        traitRepository.saveAndFlush(trait);

        // Get the trait
        restTraitMockMvc.perform(get("/api/traits/{id}", trait.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trait.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTrait() throws Exception {
        // Get the trait
        restTraitMockMvc.perform(get("/api/traits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrait() throws Exception {
        // Initialize the database
        traitRepository.saveAndFlush(trait);

        int databaseSizeBeforeUpdate = traitRepository.findAll().size();

        // Update the trait
        Trait updatedTrait = traitRepository.findById(trait.getId()).get();
        // Disconnect from session so that the updates on updatedTrait are not directly saved in db
        em.detach(updatedTrait);
        updatedTrait
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restTraitMockMvc.perform(put("/api/traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrait)))
            .andExpect(status().isOk());

        // Validate the Trait in the database
        List<Trait> traitList = traitRepository.findAll();
        assertThat(traitList).hasSize(databaseSizeBeforeUpdate);
        Trait testTrait = traitList.get(traitList.size() - 1);
        assertThat(testTrait.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTrait.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingTrait() throws Exception {
        int databaseSizeBeforeUpdate = traitRepository.findAll().size();

        // Create the Trait

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTraitMockMvc.perform(put("/api/traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trait)))
            .andExpect(status().isBadRequest());

        // Validate the Trait in the database
        List<Trait> traitList = traitRepository.findAll();
        assertThat(traitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTrait() throws Exception {
        // Initialize the database
        traitRepository.saveAndFlush(trait);

        int databaseSizeBeforeDelete = traitRepository.findAll().size();

        // Delete the trait
        restTraitMockMvc.perform(delete("/api/traits/{id}", trait.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Trait> traitList = traitRepository.findAll();
        assertThat(traitList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Trait.class);
        Trait trait1 = new Trait();
        trait1.setId(1L);
        Trait trait2 = new Trait();
        trait2.setId(trait1.getId());
        assertThat(trait1).isEqualTo(trait2);
        trait2.setId(2L);
        assertThat(trait1).isNotEqualTo(trait2);
        trait1.setId(null);
        assertThat(trait1).isNotEqualTo(trait2);
    }
}
