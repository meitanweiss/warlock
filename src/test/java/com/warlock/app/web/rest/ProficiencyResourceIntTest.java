package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.Proficiency;
import com.warlock.app.repository.ProficiencyRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProficiencyResource REST controller.
 *
 * @see ProficiencyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class ProficiencyResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ProficiencyRepository proficiencyRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProficiencyMockMvc;

    private Proficiency proficiency;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProficiencyResource proficiencyResource = new ProficiencyResource(proficiencyRepository);
        this.restProficiencyMockMvc = MockMvcBuilders.standaloneSetup(proficiencyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Proficiency createEntity(EntityManager em) {
        Proficiency proficiency = new Proficiency()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return proficiency;
    }

    @Before
    public void initTest() {
        proficiency = createEntity(em);
    }

    @Test
    @Transactional
    public void createProficiency() throws Exception {
        int databaseSizeBeforeCreate = proficiencyRepository.findAll().size();

        // Create the Proficiency
        restProficiencyMockMvc.perform(post("/api/proficiencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proficiency)))
            .andExpect(status().isCreated());

        // Validate the Proficiency in the database
        List<Proficiency> proficiencyList = proficiencyRepository.findAll();
        assertThat(proficiencyList).hasSize(databaseSizeBeforeCreate + 1);
        Proficiency testProficiency = proficiencyList.get(proficiencyList.size() - 1);
        assertThat(testProficiency.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProficiency.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createProficiencyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = proficiencyRepository.findAll().size();

        // Create the Proficiency with an existing ID
        proficiency.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProficiencyMockMvc.perform(post("/api/proficiencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proficiency)))
            .andExpect(status().isBadRequest());

        // Validate the Proficiency in the database
        List<Proficiency> proficiencyList = proficiencyRepository.findAll();
        assertThat(proficiencyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProficiencies() throws Exception {
        // Initialize the database
        proficiencyRepository.saveAndFlush(proficiency);

        // Get all the proficiencyList
        restProficiencyMockMvc.perform(get("/api/proficiencies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(proficiency.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getProficiency() throws Exception {
        // Initialize the database
        proficiencyRepository.saveAndFlush(proficiency);

        // Get the proficiency
        restProficiencyMockMvc.perform(get("/api/proficiencies/{id}", proficiency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(proficiency.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProficiency() throws Exception {
        // Get the proficiency
        restProficiencyMockMvc.perform(get("/api/proficiencies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProficiency() throws Exception {
        // Initialize the database
        proficiencyRepository.saveAndFlush(proficiency);

        int databaseSizeBeforeUpdate = proficiencyRepository.findAll().size();

        // Update the proficiency
        Proficiency updatedProficiency = proficiencyRepository.findById(proficiency.getId()).get();
        // Disconnect from session so that the updates on updatedProficiency are not directly saved in db
        em.detach(updatedProficiency);
        updatedProficiency
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restProficiencyMockMvc.perform(put("/api/proficiencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProficiency)))
            .andExpect(status().isOk());

        // Validate the Proficiency in the database
        List<Proficiency> proficiencyList = proficiencyRepository.findAll();
        assertThat(proficiencyList).hasSize(databaseSizeBeforeUpdate);
        Proficiency testProficiency = proficiencyList.get(proficiencyList.size() - 1);
        assertThat(testProficiency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProficiency.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingProficiency() throws Exception {
        int databaseSizeBeforeUpdate = proficiencyRepository.findAll().size();

        // Create the Proficiency

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProficiencyMockMvc.perform(put("/api/proficiencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proficiency)))
            .andExpect(status().isBadRequest());

        // Validate the Proficiency in the database
        List<Proficiency> proficiencyList = proficiencyRepository.findAll();
        assertThat(proficiencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProficiency() throws Exception {
        // Initialize the database
        proficiencyRepository.saveAndFlush(proficiency);

        int databaseSizeBeforeDelete = proficiencyRepository.findAll().size();

        // Delete the proficiency
        restProficiencyMockMvc.perform(delete("/api/proficiencies/{id}", proficiency.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Proficiency> proficiencyList = proficiencyRepository.findAll();
        assertThat(proficiencyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Proficiency.class);
        Proficiency proficiency1 = new Proficiency();
        proficiency1.setId(1L);
        Proficiency proficiency2 = new Proficiency();
        proficiency2.setId(proficiency1.getId());
        assertThat(proficiency1).isEqualTo(proficiency2);
        proficiency2.setId(2L);
        assertThat(proficiency1).isNotEqualTo(proficiency2);
        proficiency1.setId(null);
        assertThat(proficiency1).isNotEqualTo(proficiency2);
    }
}
