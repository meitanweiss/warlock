package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.RacialTraits;
import com.warlock.app.repository.RacialTraitsRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RacialTraitsResource REST controller.
 *
 * @see RacialTraitsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class RacialTraitsResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private RacialTraitsRepository racialTraitsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRacialTraitsMockMvc;

    private RacialTraits racialTraits;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RacialTraitsResource racialTraitsResource = new RacialTraitsResource(racialTraitsRepository);
        this.restRacialTraitsMockMvc = MockMvcBuilders.standaloneSetup(racialTraitsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RacialTraits createEntity(EntityManager em) {
        RacialTraits racialTraits = new RacialTraits()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return racialTraits;
    }

    @Before
    public void initTest() {
        racialTraits = createEntity(em);
    }

    @Test
    @Transactional
    public void createRacialTraits() throws Exception {
        int databaseSizeBeforeCreate = racialTraitsRepository.findAll().size();

        // Create the RacialTraits
        restRacialTraitsMockMvc.perform(post("/api/racial-traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(racialTraits)))
            .andExpect(status().isCreated());

        // Validate the RacialTraits in the database
        List<RacialTraits> racialTraitsList = racialTraitsRepository.findAll();
        assertThat(racialTraitsList).hasSize(databaseSizeBeforeCreate + 1);
        RacialTraits testRacialTraits = racialTraitsList.get(racialTraitsList.size() - 1);
        assertThat(testRacialTraits.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testRacialTraits.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createRacialTraitsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = racialTraitsRepository.findAll().size();

        // Create the RacialTraits with an existing ID
        racialTraits.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRacialTraitsMockMvc.perform(post("/api/racial-traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(racialTraits)))
            .andExpect(status().isBadRequest());

        // Validate the RacialTraits in the database
        List<RacialTraits> racialTraitsList = racialTraitsRepository.findAll();
        assertThat(racialTraitsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRacialTraits() throws Exception {
        // Initialize the database
        racialTraitsRepository.saveAndFlush(racialTraits);

        // Get all the racialTraitsList
        restRacialTraitsMockMvc.perform(get("/api/racial-traits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(racialTraits.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getRacialTraits() throws Exception {
        // Initialize the database
        racialTraitsRepository.saveAndFlush(racialTraits);

        // Get the racialTraits
        restRacialTraitsMockMvc.perform(get("/api/racial-traits/{id}", racialTraits.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(racialTraits.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRacialTraits() throws Exception {
        // Get the racialTraits
        restRacialTraitsMockMvc.perform(get("/api/racial-traits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRacialTraits() throws Exception {
        // Initialize the database
        racialTraitsRepository.saveAndFlush(racialTraits);

        int databaseSizeBeforeUpdate = racialTraitsRepository.findAll().size();

        // Update the racialTraits
        RacialTraits updatedRacialTraits = racialTraitsRepository.findById(racialTraits.getId()).get();
        // Disconnect from session so that the updates on updatedRacialTraits are not directly saved in db
        em.detach(updatedRacialTraits);
        updatedRacialTraits
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restRacialTraitsMockMvc.perform(put("/api/racial-traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRacialTraits)))
            .andExpect(status().isOk());

        // Validate the RacialTraits in the database
        List<RacialTraits> racialTraitsList = racialTraitsRepository.findAll();
        assertThat(racialTraitsList).hasSize(databaseSizeBeforeUpdate);
        RacialTraits testRacialTraits = racialTraitsList.get(racialTraitsList.size() - 1);
        assertThat(testRacialTraits.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testRacialTraits.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingRacialTraits() throws Exception {
        int databaseSizeBeforeUpdate = racialTraitsRepository.findAll().size();

        // Create the RacialTraits

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRacialTraitsMockMvc.perform(put("/api/racial-traits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(racialTraits)))
            .andExpect(status().isBadRequest());

        // Validate the RacialTraits in the database
        List<RacialTraits> racialTraitsList = racialTraitsRepository.findAll();
        assertThat(racialTraitsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRacialTraits() throws Exception {
        // Initialize the database
        racialTraitsRepository.saveAndFlush(racialTraits);

        int databaseSizeBeforeDelete = racialTraitsRepository.findAll().size();

        // Delete the racialTraits
        restRacialTraitsMockMvc.perform(delete("/api/racial-traits/{id}", racialTraits.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RacialTraits> racialTraitsList = racialTraitsRepository.findAll();
        assertThat(racialTraitsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RacialTraits.class);
        RacialTraits racialTraits1 = new RacialTraits();
        racialTraits1.setId(1L);
        RacialTraits racialTraits2 = new RacialTraits();
        racialTraits2.setId(racialTraits1.getId());
        assertThat(racialTraits1).isEqualTo(racialTraits2);
        racialTraits2.setId(2L);
        assertThat(racialTraits1).isNotEqualTo(racialTraits2);
        racialTraits1.setId(null);
        assertThat(racialTraits1).isNotEqualTo(racialTraits2);
    }
}
