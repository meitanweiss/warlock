package com.warlock.app.web.rest;

import com.warlock.app.WarlockApp;

import com.warlock.app.domain.Stats;
import com.warlock.app.repository.StatsRepository;
import com.warlock.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.warlock.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StatsResource REST controller.
 *
 * @see StatsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarlockApp.class)
public class StatsResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM = 1;
    private static final Integer UPDATED_NUM = 2;

    private static final String DEFAULT_ABBR = "AAAAAAAAAA";
    private static final String UPDATED_ABBR = "BBBBBBBBBB";

    @Autowired
    private StatsRepository statsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restStatsMockMvc;

    private Stats stats;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StatsResource statsResource = new StatsResource(statsRepository);
        this.restStatsMockMvc = MockMvcBuilders.standaloneSetup(statsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Stats createEntity(EntityManager em) {
        Stats stats = new Stats()
            .name(DEFAULT_NAME)
            .num(DEFAULT_NUM)
            .abbr(DEFAULT_ABBR);
        return stats;
    }

    @Before
    public void initTest() {
        stats = createEntity(em);
    }

    @Test
    @Transactional
    public void createStats() throws Exception {
        int databaseSizeBeforeCreate = statsRepository.findAll().size();

        // Create the Stats
        restStatsMockMvc.perform(post("/api/stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stats)))
            .andExpect(status().isCreated());

        // Validate the Stats in the database
        List<Stats> statsList = statsRepository.findAll();
        assertThat(statsList).hasSize(databaseSizeBeforeCreate + 1);
        Stats testStats = statsList.get(statsList.size() - 1);
        assertThat(testStats.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testStats.getNum()).isEqualTo(DEFAULT_NUM);
        assertThat(testStats.getAbbr()).isEqualTo(DEFAULT_ABBR);
    }

    @Test
    @Transactional
    public void createStatsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = statsRepository.findAll().size();

        // Create the Stats with an existing ID
        stats.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStatsMockMvc.perform(post("/api/stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stats)))
            .andExpect(status().isBadRequest());

        // Validate the Stats in the database
        List<Stats> statsList = statsRepository.findAll();
        assertThat(statsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStats() throws Exception {
        // Initialize the database
        statsRepository.saveAndFlush(stats);

        // Get all the statsList
        restStatsMockMvc.perform(get("/api/stats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stats.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].num").value(hasItem(DEFAULT_NUM)))
            .andExpect(jsonPath("$.[*].abbr").value(hasItem(DEFAULT_ABBR.toString())));
    }
    
    @Test
    @Transactional
    public void getStats() throws Exception {
        // Initialize the database
        statsRepository.saveAndFlush(stats);

        // Get the stats
        restStatsMockMvc.perform(get("/api/stats/{id}", stats.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(stats.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.num").value(DEFAULT_NUM))
            .andExpect(jsonPath("$.abbr").value(DEFAULT_ABBR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStats() throws Exception {
        // Get the stats
        restStatsMockMvc.perform(get("/api/stats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStats() throws Exception {
        // Initialize the database
        statsRepository.saveAndFlush(stats);

        int databaseSizeBeforeUpdate = statsRepository.findAll().size();

        // Update the stats
        Stats updatedStats = statsRepository.findById(stats.getId()).get();
        // Disconnect from session so that the updates on updatedStats are not directly saved in db
        em.detach(updatedStats);
        updatedStats
            .name(UPDATED_NAME)
            .num(UPDATED_NUM)
            .abbr(UPDATED_ABBR);

        restStatsMockMvc.perform(put("/api/stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStats)))
            .andExpect(status().isOk());

        // Validate the Stats in the database
        List<Stats> statsList = statsRepository.findAll();
        assertThat(statsList).hasSize(databaseSizeBeforeUpdate);
        Stats testStats = statsList.get(statsList.size() - 1);
        assertThat(testStats.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testStats.getNum()).isEqualTo(UPDATED_NUM);
        assertThat(testStats.getAbbr()).isEqualTo(UPDATED_ABBR);
    }

    @Test
    @Transactional
    public void updateNonExistingStats() throws Exception {
        int databaseSizeBeforeUpdate = statsRepository.findAll().size();

        // Create the Stats

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatsMockMvc.perform(put("/api/stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stats)))
            .andExpect(status().isBadRequest());

        // Validate the Stats in the database
        List<Stats> statsList = statsRepository.findAll();
        assertThat(statsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStats() throws Exception {
        // Initialize the database
        statsRepository.saveAndFlush(stats);

        int databaseSizeBeforeDelete = statsRepository.findAll().size();

        // Delete the stats
        restStatsMockMvc.perform(delete("/api/stats/{id}", stats.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Stats> statsList = statsRepository.findAll();
        assertThat(statsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Stats.class);
        Stats stats1 = new Stats();
        stats1.setId(1L);
        Stats stats2 = new Stats();
        stats2.setId(stats1.getId());
        assertThat(stats1).isEqualTo(stats2);
        stats2.setId(2L);
        assertThat(stats1).isNotEqualTo(stats2);
        stats1.setId(null);
        assertThat(stats1).isNotEqualTo(stats2);
    }
}
